// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

public class ClassConsts {
	public static final String FILES_ROOT = "/Android/data/com.ibme.android.oxlith/files/";
	public static final String UPLOADS_ROOT = "/Android/data/com.ibme.android.oxlith/uploads/";
	public static final String PROFILES_ROOT = "/profiles/";
	public static final String REPORTING_SUB_DIR = "reporting";
	public static final String PREFS_PRIVATE = "prefs-private";
	public static final String MOOD_ZOOM_SCHEDULE = "mood-zoom-schedule";
	public static final String BP_TEMP_SCHEDULE = "bp-temp-schedule";
	public static final String DATEFMT = "yyyy-MM-dd HH:mm:ss.SSSZ";
	public static final String EXCEPTION_FILE_DATE_FORMAT = "yyyy-MM-dd-kk-mm-ss";
	
	public static final long MILLIDAY = 24*60*60*1000L;		// Day in milliseconds
	public static final long MILLIHOUR = 60*60*1000L;
	public static final long MILLIMINUTE = 60*1000L;

	public static final double G = 9.81;
	
	public static final long LOC_TIME = 1*60*1000;			// 1 minute granularity
	public static final float LOC_DIST = 0;					// don't care about distance

	public static final String UPLOAD_ALARM = "com.ibme.android.amoss.UploadService.AlarmReceiver";
	public static final String PHONE_HOME = "com.ibme.android.amoss.UploadService.PhoneHomeReceiver";
	public static final long UPLOAD_PERIOD = 24*60*60*1000L;	// 24 hours
	public static final long PHONE_HOME_PERIOD = 1*60*60*1000L;	// 1 hours
	//public static final long PHONE_HOME_PERIOD = 1*1*10*1000L;	// 10 seconds
	public static final String UPLOAD_SIZE = "128000000"; // 128MB

	public static final int BUFFER_SIZE = 8192;
	
	public static final boolean DEBUG_DELETE_SETTINGS_ON_STARTUP = false; // Set this to true to delete the current application settings on startup. Data is not deleted.
	
	public static final int NOTIFICATION_MOOD_ZOOM = 1;
	public static final int NOTIFICATION_BP_TEMP = 2;
	public static final int NOTIFICATION_PULSE_OX = 3;
	
	public static final String URL_UPLOAD = "upload.php";
	public static final String URL_CHECK = "check.php";
	
	public static final float BATTERY_NOTIFICATION_THRESHOLD = 0.05f;
	public static final long UI_UPDATE_PERIOD = 1000; // Milliseconds
	public static final int PULSE_OX_AUTO_RECONNECT = 15; // Seconds
	
	public static final String PULSE_OX_SUB_DIR = "pulse-ox";
	public static final String PULSE_OX_DIR_DATE_FORMAT = "yyyy-MM-dd-kk-mm-ss";
	public static final String FILENAME_PPG = "ppg.dat";
	public static final String FILENAME_SPO2 = "spo2.dat";
	public static final String FILENAME_RAW = "nonin_raw.dat";
	public static final String FILENAME_RAW_INDEX = "nonin_raw_index.csv";
	
	public static final String DEFAULT_PULSE_OX_MAC_ADDRESS = "00:00:00:00:00:00";
	
	public static final String PREF_PULSE_OX_MAC_ADDRESS = "pulseOxMACAddress";
	
	public static final String PROTEUS_PATCH_DATA_DATE_FORMAT = "-yyyy-MM-dd-kk-mm-ss";
	public static final String PROTEUS_PATCH_DATA_File = "/PatchData";
}
