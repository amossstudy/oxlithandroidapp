// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

package com.ibme.android.amoss;

import com.ibme.android.oxlith.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;

import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

public class PreferenceActivityWithMenu extends PreferenceActivity {
	private int mActivityMenuId = -1;
	
	public PreferenceActivityWithMenu(int activityMenuId) {
		mActivityMenuId = activityMenuId;
	}
	
	private boolean mIsMenuFirstClick = true;
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    switch(keyCode) {
	    case KeyEvent.KEYCODE_MENU:
	        if(mIsMenuFirstClick) {
	            mIsMenuFirstClick = false;
	            ActivityCompat.invalidateOptionsMenu(this);
	        }
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_amoss, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (prefs.getBoolean("checkboxHighIntensity", false)) {
			menu.findItem(R.id.menu_settings_high_intensity).setVisible(true);
			if (prefs.getBoolean("checkboxHighIntensityBP", false) || 
					prefs.getBoolean("checkboxHighIntensityTemp", false)) {
				menu.findItem(R.id.menu_bp_temp).setVisible(true);
			} else {
				menu.findItem(R.id.menu_bp_temp).setVisible(false);
			}
			menu.findItem(R.id.menu_pulse_ox).setVisible(prefs.getBoolean("checkboxHighIntensityPulseOx", false));
		} else {
			menu.findItem(R.id.menu_settings_high_intensity).setVisible(false);
			menu.findItem(R.id.menu_bp_temp).setVisible(false);
			menu.findItem(R.id.menu_pulse_ox).setVisible(false);
		}
		
		// Custom configurations for OxLith
		menu.findItem(R.id.menu_mood_zoom).setVisible(false);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == mActivityMenuId) {
			return super.onOptionsItemSelected(item);
		} else if (itemId == R.id.menu_profile) {
			Intent profileActivity = new Intent(getBaseContext(), ActivityProfile.class);
			startActivity(profileActivity);
			return true;
		} else if (itemId == R.id.menu_mood_zoom) {
			Intent moodzoomActivity = new Intent(getBaseContext(), ActivityMoodZoom.class);
			startActivity(moodzoomActivity);
			return true;
		} else if (itemId == R.id.menu_settings) {
			Intent settingsActivity = new Intent(getBaseContext(), ActivitySettings.class);
			startActivity(settingsActivity);
			return true;
		} else if (itemId == R.id.menu_settings_admin) {
			Intent settingsActivity = new Intent(getBaseContext(), ActivitySettingsAdmin.class);
			startActivity(settingsActivity);
			return true;
		} else if (itemId == R.id.menu_settings_high_intensity) {
			Intent settingsActivity = new Intent(getBaseContext(), ActivitySettingsHighIntensity.class);
			startActivity(settingsActivity);
			return true;
		} else if (itemId == R.id.menu_about) {
			Intent aboutActivity = new Intent(getBaseContext(), ActivityAbout.class);
			startActivity(aboutActivity);
			return true;
		} else if (itemId == R.id.menu_bp_temp) {
			Intent bpActivity = new Intent(getBaseContext(), ActivityBPTemp.class);
			startActivity(bpActivity);
			return true;
		} else if (itemId == R.id.menu_pulse_ox) {
			Intent pulseOxActivity = new Intent(getBaseContext(), ActivityPulseOx.class);
			startActivity(pulseOxActivity);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}
}
