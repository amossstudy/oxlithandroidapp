// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

// In order to start the service the application should run from the phone memory.
// This one wakes up every 24 hours and attempts to upload data (if upload is enabled)
// or just removes old files.
public class ServiceUpload extends Service implements OnSharedPreferenceChangeListener {

	private static final String TAG = "AMoSSUploadService";

	private long mDeleteAge; // in milliseconds
	private String mUserID;
	private String mUserPass;
	//private boolean mUpload;
	private Calendar mUploadDisabled;
	private Calendar mReportUploadDisabled;

	ConnectivityManager mConnectivityManager;
	ConnectivityReceiver mConnectivityReceiver;
	AlarmManager mAlarmManager;
	AlarmReceiver mAlarmReceiver;
	PhoneHomeReceiver mPhoneHomeReceiver;

	@Override
	public void onCreate() {
		// TODO: Are we actually running in this thread???
		HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		// android.os.Debug.waitForDebugger();

		long ts = System.currentTimeMillis();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		int localStorage = Integer.valueOf(prefs.getString("editLocalStorage", "10")); 
		mDeleteAge = localStorage*ClassConsts.MILLIDAY;
		mUserID = prefs.getString("editUserID", "");
		mUserPass = prefs.getString("editUserPass", "");
		//mUpload = prefs.getBoolean("checkboxShare", false);
		mUploadDisabled = Calendar.getInstance();
		mReportUploadDisabled = Calendar.getInstance();

		// Prepare for connectivity tracking
		mConnectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		mConnectivityReceiver = new ConnectivityReceiver();
		registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

		// Prepare for periodic activation
		mAlarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		mAlarmReceiver = new AlarmReceiver();
		Intent intent = new Intent(ClassConsts.UPLOAD_ALARM);
		PendingIntent pintent = PendingIntent.getBroadcast(this, 0, intent, 0);
		registerReceiver(mAlarmReceiver, new IntentFilter(ClassConsts.UPLOAD_ALARM));
		mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, ts + 1000, ClassConsts.UPLOAD_PERIOD, pintent);
		
		mPhoneHomeReceiver = new PhoneHomeReceiver();
		Intent intentPH = new Intent(ClassConsts.PHONE_HOME);
		PendingIntent pintentPH = PendingIntent.getBroadcast(this, 0, intentPH, 0);
		registerReceiver(mPhoneHomeReceiver, new IntentFilter(ClassConsts.PHONE_HOME));
		mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, ts + 10000, ClassConsts.PHONE_HOME_PERIOD, pintentPH);
	}

	private File[] getFiles(final String mask) {
		return getFiles(mask, "");
	}
	
	private File[] getFiles(final String mask, final String sub_dir) {
		File[] names = new File[0];
		File root = Environment.getExternalStorageDirectory();
		// TODO: Fix it to check and wait for storage
		try {
			if (root.canRead()){
				File folder = new File(root, ClassConsts.FILES_ROOT + sub_dir);
				if (folder.exists()) {
					names = folder.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.toLowerCase().matches(mask);
						}
					});
				}
			} else {
				new ClassEvents(TAG, "ERROR", "SD card not readable");
			}
		} catch (Exception e) {
			new ClassEvents(TAG, "ERROR", "Could get files " + e.getMessage());
		}
		return names;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		new ClassEvents(TAG, "INFO", "Started");
		//new ClassReporting("service_startup", "upload", this, true);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
		prefs.registerOnSharedPreferenceChangeListener(this);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		unregisterReceiver(mConnectivityReceiver);
		unregisterReceiver(mAlarmReceiver);
		unregisterReceiver(mPhoneHomeReceiver);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
		prefs.unregisterOnSharedPreferenceChangeListener(this);

		new ClassEvents(TAG, "INFO", "Destroyed");
		//new ClassReporting("service_destroy", "upload", this, true);
	}

	private void uploadFiles() {
		File[] files;
		
		ArrayList<File> toUploadIBME = new ArrayList<File>();
		ArrayList<File> toUploadTC = new ArrayList<File>();
		
		// Upload enabled and allowed
		//if (mUpload && !mUploadDisabled) {
		if (mUploadDisabled.before(Calendar.getInstance())) {
			new ClassEvents(TAG, "INFO", "Uploading");
			// We receive multiple identical CONNECTIVITY_CHANGE events and this leads
			// to multiple upload attempts where only one is successful and others waste
			// traffic. Temporary (for one hour) disabling uploads helps to prevent it.
			mUploadDisabled = Calendar.getInstance();
			mUploadDisabled.add(Calendar.HOUR, 1);
			
			Log.i(TAG, "Uploads disabled until " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(mUploadDisabled.getTime()));
			
			files = getFiles(".*zip$");
			if (files != null) {
				for (int i = 0; i < files.length; i++) {
					toUploadIBME.add(files[i]);
				}
			}
			
			files = getFiles(".*json$");
			if (files != null) {
				for (int i = 0; i < files.length; i++) {
					toUploadTC.add(files[i]);
					new TaskUploaderTC(this).execute(files[i]);
				}
			}
		} else {
			new ClassEvents(TAG, "INFO", "Uploads disabled");
		}
		
		if (mReportUploadDisabled.before(Calendar.getInstance())) {
			mReportUploadDisabled = Calendar.getInstance();
			mReportUploadDisabled.add(Calendar.MINUTE, 1);
			
			Log.i(TAG, "Reporting uploads disabled until " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(mReportUploadDisabled.getTime()));
			
			files = getFiles(".*.upload.dat$", ClassConsts.REPORTING_SUB_DIR);
			if (files != null) {
				new TaskUploaderIBME(this, "text/csv", "reporting.dat").execute(files);
			}
		}
		
		if (toUploadIBME.size() > 0) {
			new TaskUploaderIBME(this).execute(toUploadIBME.toArray(new File[toUploadIBME.size()]));
		}
		
		if (toUploadTC.size() > 0) {
			new TaskUploaderTC(this).execute(toUploadTC.toArray(new File[toUploadTC.size()]));
		}
	}
	
	// Upload files if connectivity is available
	public class ConnectivityReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			NetworkInfo ni = mConnectivityManager.getActiveNetworkInfo();
			if (ni != null && ni.isConnected() && ni.getType() == ConnectivityManager.TYPE_WIFI) {
				new ClassEvents(TAG, "INFO", "WiFi on");

				uploadFiles();

			} else {
				// TODO: Stop uploads?
				new ClassEvents(TAG, "INFO", "WiFi off");
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static void logStorageSpace(Context context) {
		File externalStorageDir = Environment.getExternalStorageDirectory();
		StatFs statFs = new StatFs(externalStorageDir.getAbsolutePath());
		
		long total = 0L;
		long free = 0L;
		long used = 0L;
		
		try {
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
				long block_size = statFs.getBlockSize();
				long block_count = statFs.getBlockCount();
				long available_blocks = statFs.getAvailableBlocks();
				
				total = block_size * block_count;
				free = block_size * available_blocks;
			} else {
				total = statFs.getBlockSizeLong() * statFs.getBlockCountLong();
				free = statFs.getBlockSizeLong() * statFs.getAvailableBlocksLong();
			}
			
			used = total - free;
			
			new ClassReporting("external_storage_total", Long.toString(total), context, false);
			new ClassReporting("external_storage_used", Long.toString(used), context, false);
			new ClassReporting("external_storage_free", Long.toString(free), context, false);
			
			new ClassReporting("external_storage_removable", Boolean.toString(Environment.isExternalStorageRemovable()), context, false);
			
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				new ClassReporting("external_storage_emulated", Boolean.toString(Environment.isExternalStorageEmulated()), context, false);
			}
			
			new ClassReporting("external_storage_can_write", Boolean.toString(externalStorageDir.canWrite()), context, false);
		} catch (Exception e) {
			new ClassEvents(TAG, "ERROR", "Could not get external storage properties " + e.getMessage());
		}
	}
	
	// Remove old files
	public class AlarmReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			logStorageSpace(context);
			
			File[] files, recordings;
			long now = System.currentTimeMillis();

			// Delete old files
			files = getFiles(".*");
			for (int i = 0; i < files.length; i++) {
				long age = files[i].lastModified();
				if ((age > 0) && (age + mDeleteAge < now)) {
					if (files[i].delete()) {
						new ClassEvents(TAG, "INFO", "Deleted " + files[i].getName());
					} else {
						new ClassEvents(TAG, "ERROR", "Delete failed " + files[i].getName());
					}
				}
			}

			// Archive files older then 2 days
			ArrayList<File> toZip = new ArrayList<File>();
			files = getFiles(".*csv$");
			for (int i = 0; i < files.length; i++) {
				long age = files[i].lastModified();
				if ((age > 0) && (age + 2*ClassConsts.MILLIDAY < now)) {
					toZip.add(files[i]);
				}
			}
			files = getFiles("exception-.*dat$");
			for (int i = 0; i < files.length; i++) {
				toZip.add(files[i]);
			}
			
			if (toZip.size() > 0) {
				new TaskZipper().execute(toZip.toArray(new File[toZip.size()]));
			}
			
			// Archive pulse ox recordings older then 2 days
			toZip.clear();
			files = getFiles(".*", ClassConsts.PULSE_OX_SUB_DIR);
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					recordings = files[i].listFiles();
					if (recordings.length == 0) {
						long age = files[i].lastModified();
						if ((age > 0) && (age + 2*ClassConsts.MILLIDAY < now)) {
							if (files[i].delete()) {
								new ClassEvents(TAG, "INFO", "Deleted " + files[i].getPath());
							} else {
								new ClassEvents(TAG, "ERROR", "Delete failed " + files[i].getPath());
							}
						}
					} else {
						long mostRecent = 0;
						for (int j = 0; j < recordings.length; j++) {
							long age = recordings[j].lastModified();
							if (age > mostRecent) {
								mostRecent = age;
							}
						}
						if ((mostRecent > 0) && (mostRecent + 2*ClassConsts.MILLIDAY < now)) {
							toZip.add(files[i]);
						}
					}
				}
			}
			
			if (toZip.size() > 0) {
				new TaskFolderZipper(new File(Environment.getExternalStorageDirectory().toString() + ClassConsts.FILES_ROOT)).execute(toZip.toArray(new File[toZip.size()]));
			}

			// Prepare profiles for upload
			if (!TextUtils.isEmpty(mUserID)) {
				files = new ClassProfileAccelerometry(context).getUploads();
				for (int i = 0; i < files.length; i++) {
					File root = Environment.getExternalStorageDirectory();
					File folder = new File(root, ClassConsts.FILES_ROOT);
					File dst = new File(folder, files[i].getName());
					ArrayList<ClassProfileAccelerometry.Values> ivals = new ArrayList<ClassProfileAccelerometry.Values>();
					ArrayList<TaskUploaderTC.Values> ovals = new ArrayList<TaskUploaderTC.Values>();
					String tcid = mUserID.substring(2);
					try {
						// Read
						ivals = new ClassProfileAccelerometry(context).readVals(files[i]);

						// Convert
						for (ClassProfileAccelerometry.Values v : ivals) {
							ovals.add(new TaskUploaderTC.Values(tcid, v.t, v.l));
						}

						// Write
						FileOutputStream ostream = new FileOutputStream(dst);
						Gson gson = new Gson();
						JsonWriter writer = new JsonWriter(new OutputStreamWriter(ostream, "UTF-8"));
						writer.setIndent(" ");
						writer.beginArray();
						for (TaskUploaderTC.Values v : ovals) {
							gson.toJson(v, TaskUploaderTC.Values.class, writer);
						}
						writer.endArray();
						writer.close();
					} catch (IOException e) {
						new ClassEvents(TAG, "ERROR", "Couldn't copy " + dst.getAbsolutePath());
					}
				}
			}
		}
	}

	// Periodically phone home if possible
	public class PhoneHomeReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			NetworkInfo mWifi = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			Log.i(TAG, "Phoning Home");
			if (mWifi.isConnected()) {
				new ClassEvents(TAG, "INFO", "WiFi Connected");
				
				new TaskCheckerIBME(getApplicationContext(), true).execute();
				
				// Attempt to upload files
				uploadFiles();
			}
		}
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Messages interface (unused)
	///////////////////////////////////////////////////////////////////////////
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;

	private ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Shared preference change listener
	///////////////////////////////////////////////////////////////////////////
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals("editLocalStorage")) {
			int localStorage = Integer.valueOf(sharedPreferences.getString(key, "10")); 
			if (localStorage > 0) {
				mDeleteAge = localStorage*ClassConsts.MILLIDAY;
			}
		} else if (key.equals("editUserID")) {
			mUserID = sharedPreferences.getString("editUserID", "");
		} else if (key.equals("editUserPass")) {
			mUserPass = sharedPreferences.getString("editUserPass", "");
		} /*else if (key.equals("checkboxShare")) {
			mUpload = sharedPreferences.getBoolean("checkboxShare", false);
		}*/
	}
}