// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

package com.ibme.android.amoss;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ibme.android.oxlith.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Log;

public class VersionManager {
	
	private static final String TAG = "AMoSSVersionManager";
	
	public static void checkForUpgrades(Context context) {
		checkForUpgrades(context, false);
	}
	public static void checkForUpgrades(Context context, boolean bUploadSettings) {
		SharedPreferences prefs = context.getSharedPreferences(
		        ClassConsts.PREFS_PRIVATE, Context.MODE_PRIVATE);
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor;
		
		// For debug use only!
		//editor = settings.edit();
		//editor.clear();
		//editor.commit();
		//editor = null;
		
		// Initialize preferences
		PreferenceManager.setDefaultValues(context, R.xml.preferences, true);
		PreferenceManager.setDefaultValues(context, R.xml.preferences_admin, true);
		PreferenceManager.setDefaultValues(context, R.xml.preferences_high_intensity, true);
		
		int iPreviousVersion = prefs.getInt("currentVersion", 0);
		int iCurrentVersion = 0;
		
		PackageInfo pinfo;
		try {
			pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			iCurrentVersion = pinfo.versionCode;
		} catch (NameNotFoundException e) {
			// meh
		}
		
		if (bUploadSettings) {
			new ClassReporting("setting_high_intensity", Boolean.toString(settings.getBoolean("checkboxHighIntensity", false)), context, false);
			new ClassReporting("setting_high_intensity_pulse_ox", Boolean.toString(settings.getBoolean("checkboxHighIntensityPulseOx", false)), context, false);
			new ClassReporting("setting_share_activity_light", Boolean.toString(settings.getBoolean("checkboxShare", true)), context, false);
			new ClassReporting("setting_share_location", Boolean.toString(settings.getBoolean("checkboxLocation", true)), context, false);
			new ClassReporting("setting_share_calls_texts", Boolean.toString(settings.getBoolean("checkboxComm", true)), context, false);
			new ClassReporting("setting_share_mood_zoom", Boolean.toString(settings.getBoolean("checkboxMoodZoom", true)), context, false);
			new ClassReporting("setting_phone_location", settings.getString("listMobileUsage", "1"), context, false);
			new ClassReporting("setting_high_intensity_mood_zoom", Boolean.toString(settings.getBoolean("checkboxHighIntensityMoodZoom", true)), context, false);
			new ClassReporting("setting_high_intensity_bp", Boolean.toString(settings.getBoolean("checkboxHighIntensityBP", true)), context, false);
			new ClassReporting("setting_high_intensity_temp", Boolean.toString(settings.getBoolean("checkboxHighIntensityTemp", true)), context, false);
			new ClassReporting("setting_local_storage", settings.getString("editLocalStorage", ""), context, false);
			new ClassReporting("setting_mood_zoom_time", Long.toString(settings.getLong("timeDailyMoodZoom", -1)), context, false);
			new ClassReporting("setting_high_intensity_mood_zoom_time_1", Long.toString(settings.getLong("timeHighIntensityMoodZoomStart", -1)), context, false);
			new ClassReporting("setting_high_intensity_mood_zoom_time_2", Long.toString(settings.getLong("timeHighIntensityMoodZoomEnd", -1)), context, false);
			new ClassReporting("setting_high_intensity_bp_temp_reminder", Boolean.toString(settings.getBoolean("checkboxHighIntensityBPTempReminder", true)), context, false);
			new ClassReporting("setting_high_intensity_bp_temp_time_1", Long.toString(settings.getLong("timeHighIntensityBPTempMorning", -1)), context, false);
			new ClassReporting("setting_high_intensity_bp_temp_time_2", Long.toString(settings.getLong("timeHighIntensityBPTempAfternoon", -1)), context, false);
			new ClassReporting("setting_high_intensity_bp_temp_time_3", Long.toString(settings.getLong("timeHighIntensityBPTempEvening", -1)), context, false);
			
			TelephonyManager telMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			int simState = telMgr.getSimState();
			switch (simState) {
				case TelephonyManager.SIM_STATE_ABSENT:
					new ClassReporting("sim_state", "SIM_STATE_ABSENT", context, false);
					break;
				case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
					new ClassReporting("sim_state", "SIM_STATE_NETWORK_LOCKED", context, false);
					break;
				case TelephonyManager.SIM_STATE_PIN_REQUIRED:
					new ClassReporting("sim_state", "SIM_STATE_PIN_REQUIRED", context, false);
					break;
				case TelephonyManager.SIM_STATE_PUK_REQUIRED:
					new ClassReporting("sim_state", "SIM_STATE_PUK_REQUIRED", context, false);
					break;
				case TelephonyManager.SIM_STATE_READY:
					new ClassReporting("sim_state", "SIM_STATE_READY", context, false);
					break;
				case TelephonyManager.SIM_STATE_UNKNOWN:
					new ClassReporting("sim_state", "SIM_STATE_UNKNOWN", context, false);
					break;
			}

			ReceiverLocationProviders.logProviderState(context);
			ServiceUpload.logStorageSpace(context);
			
			SimpleDateFormat fmt = new SimpleDateFormat(ClassConsts.DATEFMT);
			
			long lDate = prefs.getLong("dateInstalled", 0);
			if (lDate > 0) {
				new ClassReporting("installed", fmt.format(new Date(lDate)), context, false);
			}
			
			lDate = prefs.getLong("dateHighIntensityActivated", 0);
			if (lDate > 0) {
				new ClassReporting("setting_high_intensity_activated_date", fmt.format(new Date(lDate)), context, false);
			}
		}
		
		editor = settings.edit();
		
		// For debug use only!
		//iCurrentVersion = 1019;
		//iPreviousVersion = 1018;

		Log.i("checkForUpgrades", "Previous Version: " + iPreviousVersion);
		Log.i("checkForUpgrades", "Current Version: " + iCurrentVersion);
		
		if ((iCurrentVersion != iPreviousVersion) && (iPreviousVersion != 0)) {
			new ClassEvents(TAG, "INFO", "Upgrading from version " + iPreviousVersion + " to " + iCurrentVersion);
			// Always report version upgrade
			//if (bUploadSettings) {
				new ClassReporting("version_upgrade_from", Integer.toString(iPreviousVersion), context, false);
				new ClassReporting("version", Integer.toString(iCurrentVersion), context, !bUploadSettings);
			//}
		} else if (iPreviousVersion == 0) {
			new ClassEvents(TAG, "INFO", "Fresh install of version " + iCurrentVersion);
		} else {
			new ClassEvents(TAG, "INFO", "Running version " + iCurrentVersion);
		}
		
		if (bUploadSettings) {
			new ClassReporting("version", Integer.toString(iCurrentVersion), context, true);
		}
		
		if ((iCurrentVersion >= 1014) && (iPreviousVersion <= 1013)) {
			Uri defaultRingtone;
			if (iPreviousVersion == 0) {
				defaultRingtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			} else {
				defaultRingtone = RingtoneManager.getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_ALARM);
			}
			editor.putString("listNotificationSound", defaultRingtone.toString());
		}
		
		if ((iCurrentVersion >= 1017) && (iPreviousVersion <= 1016)) {
			if (iPreviousVersion > 0) {
				if (settings.getBoolean("checkboxHighIntensity", false)) {
					editor.putBoolean("checkboxHighIntensityBPTempReminder", false);
				}
			}
		}
		
		if ((iCurrentVersion >= 1018) && (iPreviousVersion <= 1017) && (iPreviousVersion > 0)) {
			editor.putString("editServerRaw", context.getResources().getString(R.string.defaultServerRaw));
		}
		
		if ((iCurrentVersion >= 1019) && (iPreviousVersion <= 1018) && (iPreviousVersion > 0)) {
			// There was a bug in versions 1018 and below that meant that even if
			// activity recording was disabled the data was still being written to
			// the data files just not uploaded. The check was then made at the
			// upload stage, but this also meant that if activity sharing was
			// re-enabled then previously recorded data would still be sent. In
			// version 1019 this is fixed, but we need to delete any activity /
			// light data stored if activity recording is disabled.
			if (settings.getBoolean("checkboxHighIntensity", false)) {
				if (!settings.getBoolean("checkboxShare", true)) {
					new ClassEvents(TAG, "INFO", "Activity monitoring disabled so deleting old activity and light files");
					File[] files;
					
					files = getFiles("activity-*.*");
					if (files != null) {
						for (int i = 0; i < files.length; i++) {
							new ClassEvents(TAG, "INFO", "Deleting file " + files[i].getName());
							if (!files[i].delete()) {
								new ClassEvents(TAG, "INFO", "Error deleting file " + files[i].getName());
							}
						}
					}
					files = getFiles("light-*.*");
					if (files != null) {
						for (int i = 0; i < files.length; i++) {
							new ClassEvents(TAG, "INFO", "Deleting file " + files[i].getName());
							if (!files[i].delete()) {
								new ClassEvents(TAG, "INFO", "Error deleting file " + files[i].getName());
							}
						}
					}
				}
			}
		}
		
		if (iCurrentVersion >= 1034) {
			editor.putString("editServerRaw", context.getResources().getString(R.string.defaultServerRaw));
			editor.putString("editServerTC", context.getResources().getString(R.string.defaultServerTC));
		}
		
		editor.commit();
		
		editor = prefs.edit();
		editor.putInt("currentVersion", iCurrentVersion);
		editor.commit();
	}
	
	private static File[] getFiles(final String mask) {
		File[] names = new File[0];
		File root = Environment.getExternalStorageDirectory();
		// TODO: Fix it to check and wait for storage
		try {
			if (root.canRead()){
				File folder = new File(root, ClassConsts.FILES_ROOT);
				if (folder.exists()) {
					names = folder.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.toLowerCase().matches(mask);
						}
					});
				}
			} else {
				new ClassEvents(TAG, "ERROR", "SD card not readable");
			}
		} catch (Exception e) {
			new ClassEvents(TAG, "ERROR", "Could get files " + e.getMessage());
		}
		return names;
	}
}
