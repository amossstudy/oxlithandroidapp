// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

import com.ibme.android.oxlith.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;

// In order to start the service the application should run from the phone memory 
public class ServiceCollect extends Service implements
	SensorEventListener, OnSharedPreferenceChangeListener {

	// TODO: Make it a foreground service

	private static final String TAG = "AMoSSCollectService";

	private SensorManager mSensorManager;
	private Sensor mSensorAccelerometer;
	private Sensor mSensorLight;

	private ClassAccelerometry mAccelerometry;
	private ClassLight mLight;
	private ClassLocationListenerStandard mLocationStandard;
	private ClassLocationListenerGMS mLocationGms;
	private ClassCallsTexts mCallsTexts;
	private ClassProfileAccelerometry mProfile;
	private ClassBattery mBattery;
	
	private boolean mRecordActivity;

	private BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// Stop recording if battery level is less than 5%.
			int batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
			int batteryScale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
			float percentage = (float) batteryLevel / (float) batteryScale;
			long ts = System.currentTimeMillis();
			
			mBattery.update(ts, percentage);
		}
	};
	
	private BroadcastReceiver wifiStateReceiver = new BroadcastReceiver() {

		private static final String TAG = "ReceiverWiFi";

		@Override
		public void onReceive(Context context, Intent intent) {
			int extraWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
			
			switch(extraWifiState){
				case WifiManager.WIFI_STATE_DISABLED:
					new ClassReporting("wifi_state", "disabled", context, true);
					new ClassEvents(TAG, "INFO", "WIFI STATE DISABLED");
					break;
				case WifiManager.WIFI_STATE_DISABLING:
					new ClassEvents(TAG, "INFO", "WIFI STATE DISABLING");
					break;
				case WifiManager.WIFI_STATE_ENABLED:
					new ClassReporting("wifi_state", "enabled", context, true);
					new ClassEvents(TAG, "INFO", "WIFI STATE ENABLED");
					break;
				case WifiManager.WIFI_STATE_ENABLING:
					new ClassEvents(TAG, "INFO", "WIFI STATE ENABLING");
					break;
				case WifiManager.WIFI_STATE_UNKNOWN:
					new ClassEvents(TAG, "INFO", "WIFI STATE UNKNOWN");
					break;
			}
		}
	};
	
	@Override
	public void onCreate() {
		// TODO: Are we actually running in this thread???
		HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		// android.os.Debug.waitForDebugger();

		// Values should match android definitions
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		int sdelay = SensorManager.SENSOR_DELAY_NORMAL;
		
		Resources r = getResources();
		
		String[] a;
		String v;

		// WARNING: For arrays below indexing starts from 1, so substract
		a = r.getStringArray(R.array.usageArray);
		v = a[Integer.valueOf(prefs.getString("listMobileUsage", "1")) - 1];
		new ClassEvents(TAG, "INFO", "Phone usage: " + v);
		
		/* Removed settings from app
		a = r.getStringArray(R.array.samplingArray);
		int sdelay = Integer.valueOf(prefs.getString("listSamplingRate", "3"));
		v = a[sdelay];
		new ClassEvents(TAG, "INFO", "Sampling rate: " + v);

		v = prefs.getString("editUserBday", "");
		new ClassEvents(TAG, "INFO", "Birth year: " + v);

		a = r.getStringArray(R.array.genderArray);
		v = a[Integer.valueOf(prefs.getString("listUserGender", "1")) - 1];
		new ClassEvents(TAG, "INFO", "Gender: " + v);

		a = r.getStringArray(R.array.statusArray);
		v = a[Integer.valueOf(prefs.getString("listUserStatus", "1")) - 1];
		new ClassEvents(TAG, "INFO", "Occupation: " + v);
		*/
		
		mRecordActivity = prefs.getBoolean("checkboxShare", false);
		boolean loc = prefs.getBoolean("checkboxLocation", false);
		boolean comm = prefs.getBoolean("checkboxComm", false);
		//boolean mz = prefs.getBoolean("checkboxMoodZoom", false);

		long ts = System.currentTimeMillis();
		
		if (mRecordActivity) {
			mAccelerometry = new ClassAccelerometry(this);
			mAccelerometry.init(ts);
			mLight = new ClassLight(this);
			mLight.init(ts);
		}
		
		mProfile = new ClassProfileAccelerometry(this);
		mProfile.init(ts);

		// Get phone model and android version
		new ClassEvents(TAG, "INFO", "Phone: " + Build.MANUFACTURER + " " + Build.MODEL + " " + Build.VERSION.RELEASE);

		// Get sensors
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		String str = "Sensors: ";
		List<Sensor> lst = mSensorManager.getSensorList(Sensor.TYPE_ALL);
		Iterator<Sensor> itr = lst.iterator();
		while (itr.hasNext()) {
			str = str + ", " + itr.next().getName();
		}
		new ClassEvents(TAG, "INFO", str);
		
		mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if (mSensorAccelerometer != null) {
			new ClassEvents(TAG, "INFO", "AccelerometerType: " + mSensorAccelerometer.getName());
			new ClassEvents(TAG, "INFO", "AccelerometerMax: " + mSensorAccelerometer.getMaximumRange());
			new ClassEvents(TAG, "INFO", "AccelerometerResolution: " + mSensorAccelerometer.getResolution());
			mSensorManager.registerListener(this, mSensorAccelerometer, sdelay);
		}
		
		mSensorLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		if (mSensorLight != null) {
			new ClassEvents(TAG, "INFO", "LightType: " + mSensorLight.getName());
			new ClassEvents(TAG, "INFO", "LightMax: " + mSensorLight.getMaximumRange());
			new ClassEvents(TAG, "INFO", "LightResolution: " + mSensorLight.getResolution());
			mSensorManager.registerListener(this, mSensorLight, sdelay);
		}
		
		if (loc) {
			mLocationStandard = new ClassLocationListenerStandard(this);
			mLocationStandard.init(ts);
			mLocationGms = new ClassLocationListenerGMS(this);
			mLocationGms.init(ts);
		}

		if (comm) {
			mCallsTexts = new ClassCallsTexts(this);
			mCallsTexts.init(ts);
		}

		//if (mz) {
			CreateMoodZoomSchedule(this, prefs);
			ScheduleMoodZoom(this);
			
			CreateBPTempSchedule(this, prefs);
			ScheduleBPTemp(this);
		//}
		
		// Battery check receiver.
		mBattery = new ClassBattery(this);
		mBattery.init(ts);
		
		registerReceiver(this.batteryLevelReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		
		registerReceiver(this.wifiStateReceiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		new ClassEvents(TAG, "INFO", "Started");
		//new ClassReporting("service_startup", "collect", this, true);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefs.registerOnSharedPreferenceChangeListener(this);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		if (mCallsTexts != null) {
			mCallsTexts.fini();
		}
		if (mLocationStandard != null) {
			mLocationStandard.fini();
		}
		if (mLocationGms != null) {
			mLocationGms.fini();
		}
		mSensorManager.unregisterListener(this);
		mSensorManager.unregisterListener(this);
		if (mLight != null) {
			mLight.fini();
		}
		if (mAccelerometry != null) {
			mAccelerometry.fini();
		}
		mProfile.fini();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefs.unregisterOnSharedPreferenceChangeListener(this);

		unregisterReceiver(batteryLevelReceiver);
		
		if (mBattery != null) {
			mBattery.fini();
		}
		
		unregisterReceiver(wifiStateReceiver);
		
		new ClassEvents(TAG, "INFO", "Destroyed");
		//new ClassReporting("service_destroy", "collect", this, true);
	}

	///////////////////////////////////////////////////////////////////////////
	// Messages interface
	///////////////////////////////////////////////////////////////////////////
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_UNREGISTER_CLIENT = 2;

	private ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

//	private void sendRefreshProgress(String status)
//	{
//		for (int i=mClients.size()-1; i>=0; i--) {
//			try {
//				Message m = Message.obtain(null, MSG_REFRESH_PROGRESS, 0, 0);
//				Bundle b = new Bundle();
//				b.putString("status", status);
//				m.setData(b);
//				mClients.get(i).send(m);
//			} catch (RemoteException e) {
//				mClients.remove(i);
//			}
//		}
//	}

	///////////////////////////////////////////////////////////////////////////
	// Sensors interface
	///////////////////////////////////////////////////////////////////////////
	@Override
	public void onSensorChanged(SensorEvent event) {
		// event.timestamp has a different meaning on different android versions
		long ts = System.currentTimeMillis();

		int type = event.sensor.getType();
		if (type == Sensor.TYPE_ACCELEROMETER) {
			float x = event.values[0];
			float y = event.values[1];
			float z = event.values[2];
			if ((mRecordActivity) && (mAccelerometry != null)) {
				mAccelerometry.update(ts, x, y, z);
			}
			mProfile.update(ts, x, y, z);
		} else if (type == Sensor.TYPE_LIGHT) {
			float lux = event.values[0];
			if ((mRecordActivity) && (mLight != null)) {
				mLight.update(ts, lux);
			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO: What's that?
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		long ts = System.currentTimeMillis();
		
		if (key.equals("checkboxHighIntensity")) {
			new ClassReporting("setting_high_intensity", Boolean.toString(prefs.getBoolean("checkboxHighIntensity", false)), this, true);
		} else if (key.equals("checkboxHighIntensityPulseOx")) {
			new ClassReporting("setting_high_intensity_pulse_ox", Boolean.toString(prefs.getBoolean("checkboxHighIntensityPulseOx", false)), this, true);
		} else if (key.equals("checkboxShare")) {
			new ClassReporting("setting_share_activity_light", Boolean.toString(prefs.getBoolean("checkboxShare", true)), this, true);
		} else if (key.equals("checkboxLocation")) {
			new ClassReporting("setting_share_location", Boolean.toString(prefs.getBoolean("checkboxLocation", true)), this, true);
		} else if (key.equals("checkboxComm")) {
			new ClassReporting("setting_share_calls_texts", Boolean.toString(prefs.getBoolean("checkboxComm", true)), this, true);
		} else if (key.equals("checkboxMoodZoom")) {
			new ClassReporting("setting_share_mood_zoom", Boolean.toString(prefs.getBoolean("checkboxMoodZoom", true)), this, true);
		} else if (key.equals("listMobileUsage")) {
			new ClassReporting("setting_phone_location", prefs.getString("listMobileUsage", "1"), this, true);
		} else if (key.equals("checkboxHighIntensityMoodZoom")) {
			new ClassReporting("setting_high_intensity_mood_zoom", Boolean.toString(prefs.getBoolean("checkboxHighIntensityMoodZoom", true)), this, true);
		} else if (key.equals("checkboxHighIntensityBP")) {
			new ClassReporting("setting_high_intensity_bp", Boolean.toString(prefs.getBoolean("checkboxHighIntensityBP", true)), this, true);
		} else if (key.equals("checkboxHighIntensityTemp")) {
			new ClassReporting("setting_high_intensity_temp", Boolean.toString(prefs.getBoolean("checkboxHighIntensityTemp", true)), this, true);
		} else if (key.equals("editLocalStorage")) {
			new ClassReporting("setting_local_storage", prefs.getString("editLocalStorage", ""), this, true);
		} else if (key.equals("timeDailyMoodZoom")) {
			new ClassReporting("setting_mood_zoom_time", Long.toString(prefs.getLong("timeDailyMoodZoom", -1)), this, true);
		} else if (key.equals("timeHighIntensityMoodZoomStart")) {
			new ClassReporting("setting_high_intensity_mood_zoom_time_1", Long.toString(prefs.getLong("timeHighIntensityMoodZoomStart", -1)), this, true);
		} else if (key.equals("timeHighIntensityMoodZoomEnd")) {
			new ClassReporting("setting_high_intensity_mood_zoom_time_2", Long.toString(prefs.getLong("timeHighIntensityMoodZoomEnd", -1)), this, true);
		} else if (key.equals("checkboxHighIntensityBPTempReminder")) {
			new ClassReporting("setting_high_intensity_bp_temp_reminder", Boolean.toString(prefs.getBoolean("checkboxHighIntensityBPTempReminder", true)), this, true);
		} else if (key.equals("timeHighIntensityBPTempMorning")) {
			new ClassReporting("setting_high_intensity_bp_temp_time_1", Long.toString(prefs.getLong("timeHighIntensityBPTempMorning", -1)), this, true);
		} else if (key.equals("timeHighIntensityBPTempAfternoon")) {
			new ClassReporting("setting_high_intensity_bp_temp_time_2", Long.toString(prefs.getLong("timeHighIntensityBPTempAfternoon", -1)), this, true);
		} else if (key.equals("timeHighIntensityBPTempEvening")) {
			new ClassReporting("setting_high_intensity_bp_temp_time_3", Long.toString(prefs.getLong("timeHighIntensityBPTempEvening", -1)), this, true);
		}
		
		if (key.equals("listSamplingRate")) {
			String sdelay = prefs.getString(key, "3");
			new ClassEvents(TAG, "INFO", "Sampling rate " + sdelay);
			mSensorManager.unregisterListener(this);
			mSensorManager.unregisterListener(this);
			mSensorManager.registerListener(this, mSensorAccelerometer, Integer.valueOf(sdelay));
			mSensorManager.registerListener(this, mSensorLight, Integer.valueOf(sdelay));
		} else if (key.equals("checkboxShare")) {
			mRecordActivity = prefs.getBoolean("checkboxShare", false);
			if (mRecordActivity) {
				if (mLight != null) {
					mLight.fini();
				}
				if (mAccelerometry != null) {
					mAccelerometry.fini();
				}
				mAccelerometry = new ClassAccelerometry(this);
				mAccelerometry.init(ts);
				mLight = new ClassLight(this);
				mLight.init(ts);
			} else {
				new ClassEvents(TAG, "INFO", "Stopping Light");
				if (mLight != null) {
					mLight.fini();
					mLight = null;
				}
				new ClassEvents(TAG, "INFO", "Stopping Activity");
				if (mAccelerometry != null) {
					mAccelerometry.fini();
					mAccelerometry = null;
				}
			}
		} else if (key.equals("checkboxLocation")) {
			boolean loc = prefs.getBoolean("checkboxLocation", false);
			if (loc) {
				mLocationStandard = new ClassLocationListenerStandard(this);
				mLocationStandard.init(ts);
				mLocationGms = new ClassLocationListenerGMS(this);
			} else {
				new ClassEvents(TAG, "INFO", "Stopping Location");
				if (mLocationStandard != null) {
					mLocationStandard.fini();
				}
				if (mLocationGms != null) {
					mLocationGms.fini();
				}
			}
		} else if (key.equals("checkboxComm")) {
			boolean loc = prefs.getBoolean("checkboxComm", false);
			if (loc) {
				mCallsTexts = new ClassCallsTexts(this);
				mCallsTexts.init(ts);
			} else {
				new ClassEvents(TAG, "INFO", "Stopping SMS");
				if (mCallsTexts != null) {
					mCallsTexts.fini();
				}
			}
		} 
		
		if (key.equals("checkboxMoodZoom") || key.equals("timeDailyMoodZoom") ||
				   key.equals("checkboxHighIntensity") || key.equals("checkboxHighIntensityMoodZoom") ||
				   key.equals("timeHighIntensityMoodZoomStart") || key.equals("timeHighIntensityMoodZoomEnd")) {
			
			CreateMoodZoomSchedule(this, prefs);
			ScheduleMoodZoom(this);
		}

		if (key.equals("checkboxHighIntensityBP") || key.equals("checkboxHighIntensityTemp") ||
				   key.equals("checkboxHighIntensity") || key.equals("checkboxHighIntensityBPTempReminder") ||
				   key.equals("timeHighIntensityBPTempMorning") || key.equals("timeHighIntensityBPTempAfternoon") ||
				   key.equals("timeHighIntensityBPTempEvening")) {
			
			CreateBPTempSchedule(this, prefs);
			ScheduleBPTemp(this);
		}
	}

	public static void CreateMoodZoomSchedule(Context context, SharedPreferences prefs) {
		boolean bMZ_Enabled = prefs.getBoolean("checkboxMoodZoom", true);
		boolean bHIMZ_Enabled = prefs.getBoolean("checkboxHighIntensity", false) && 
								prefs.getBoolean("checkboxHighIntensityMoodZoom", false);
		long lMZ_Schedule = prefs.getLong("timeDailyMoodZoom", 68400000);
		long lHIMZ_Start = prefs.getLong("timeHighIntensityMoodZoomStart", 32400000);
		long lHIMZ_End = prefs.getLong("timeHighIntensityMoodZoomEnd", 75600000);
		
		SharedPreferences schedule = context.getSharedPreferences(
		        ClassConsts.MOOD_ZOOM_SCHEDULE, Context.MODE_PRIVATE);
		
		//long lMoodZoomType = schedule.getLong("typeMoodZoom", 0); // 0: disabled, 1: daily, 2: high intensity
		//long lDateSchedule = schedule.getLong("dateSchedule", 0);
		
		//Date dateSchedule = new Date(lDateSchedule);
		
		Calendar dNow = Calendar.getInstance();
		Calendar cNow = Calendar.getInstance();
		cNow.clear();
		cNow.set(dNow.get(Calendar.YEAR), dNow.get(Calendar.MONTH), dNow.get(Calendar.DAY_OF_MONTH));
		
		
		SharedPreferences.Editor editor = schedule.edit();
		
		
		if (bHIMZ_Enabled) {
			editor.putLong("typeMoodZoom", 2); // 2: high intensity
			
			long lDiff = lHIMZ_End - lHIMZ_Start;
			double dDiff = (double)lDiff / 9;
			
			long[] lScheduleTime = new long[10];
			Calendar[] cScheduleTime = new Calendar[10];
			
			for (int i = 0; i < 10; i++) {
				lScheduleTime[i] = Math.round(lHIMZ_Start + (i * dDiff));
				cScheduleTime[i] = Calendar.getInstance();
				cScheduleTime[i].setTimeInMillis(lScheduleTime[i]);
			}
			
			Calendar cLastSchedule = CombineDateTime(cNow, cScheduleTime[9]);
			
			if (!cLastSchedule.after(dNow)) {
				cNow.add(Calendar.DATE, 1);
			}
			
			for (int i = 0; i < 10; i++) {
				Calendar cSchedule = CombineDateTime(cNow, cScheduleTime[i]);
				editor.putLong("dateSchedule" + Integer.toString(i + 1), cSchedule.getTimeInMillis());
				Log.i("CreateMoodZoomSchedule", "Added schedule: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cSchedule.getTime()));
			}
			
		} else if (bMZ_Enabled) {
			editor.putLong("typeMoodZoom", 1); // 1: daily
			
			Calendar cMZ_Schedule = Calendar.getInstance();
			cMZ_Schedule.setTimeInMillis(lMZ_Schedule);
			Calendar cSchedule = CombineDateTime(cNow, cMZ_Schedule);
			
			if (!cSchedule.after(dNow)) {
				cSchedule.add(Calendar.DATE, 1);
				cNow.add(Calendar.DATE, 1);
			}
			
			editor.putLong("dateSchedule1", cSchedule.getTimeInMillis());
			Log.i("CreateMoodZoomSchedule", "Added schedule: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cSchedule.getTime()));
		} else {
			editor.putLong("typeMoodZoom", 0); // 0: disabled
		}
		
		editor.putLong("dateSchedule", cNow.getTimeInMillis());
		
		editor.commit();
		
		//if ((lDateSchedule == 0) || (!dateSchedule.equals(c.getTime()))) {
		//	//definitely create new schedule for today
		//} else {
		//	
		//}
		//dSchedule.
		
	}
	
	private static Calendar CombineDateTime(Calendar date, Calendar time) {
		Calendar cCombined = (Calendar)date.clone();
		cCombined.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
		cCombined.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
		cCombined.set(Calendar.SECOND, time.get(Calendar.SECOND));
		cCombined.set(Calendar.MILLISECOND, time.get(Calendar.MILLISECOND));
		return cCombined;
	}
	
	public static void ScheduleMoodZoom(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		SharedPreferences schedule = context.getSharedPreferences(
		        ClassConsts.MOOD_ZOOM_SCHEDULE, Context.MODE_PRIVATE);
		
		long lMoodZoomType = schedule.getLong("typeMoodZoom", 0); // 0: disabled, 1: daily, 2: high intensity
		
		/*boolean alarm = prefs.getBoolean("checkboxMoodZoom", false);
		if (!alarm) {
			new ClassEvents(TAG, "INFO", "Skipping scheduling, alarm disabled");
			return;
		}*/
		
		// Stop alarm
		//if ((lMoodZoomType == 0) || app_restart) {
			new ClassEvents(TAG, "INFO", "Stopping alarm");
			AlarmManager a = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(context, ReceiverMoodZoom.class);
			PendingIntent pending = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE);
			if (pending != null) {
				a.cancel(pending);
			}

		//}

		// Schedule a new alarm only if necessary
		//Intent intent = new Intent(context, ReceiverMoodZoom.class);
		//PendingIntent pending = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE);
		//if (pending != null && app_restart) {
		//	new ClassEvents(TAG, "INFO", "Skipping scheduling, alarm set");
		//	return;
		//}

		long lSchedule = 0;
		
		if (lMoodZoomType == 0) {
			return;
		} else if (lMoodZoomType == 1) {
			lSchedule = schedule.getLong("dateSchedule1", 0);
		} else if (lMoodZoomType == 2) {
			Calendar cSchedule = Calendar.getInstance();
			for (int i = 0; i < 10; i++) {
				lSchedule = schedule.getLong("dateSchedule" + Integer.toString(i + 1), 0);
				cSchedule.setTimeInMillis(lSchedule);
				
				if (cSchedule.after(Calendar.getInstance())) {
					break;
				}
				lSchedule = 0;
			}
		}
		
		if (lSchedule == 0) {
			Log.i("ScheduleMoodZoom", "No valid schedule found. Rescheduling.");
			CreateMoodZoomSchedule(context, prefs);
			ScheduleMoodZoom(context);
			return;
		}
			
		
		//AlarmManager a = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		pending = PendingIntent.getBroadcast(context, 0, intent, 0);
		
		/*long lTime = prefs.getLong("timeDailyMoodZoom", 0);

		Calendar c = Calendar.getInstance();
		Calendar cTime = Calendar.getInstance();
		//Random r = new Random();
		cTime.setTimeInMillis(lTime);
		
		c.set(Calendar.HOUR_OF_DAY, cTime.get(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, cTime.get(Calendar.MINUTE));
		c.set(Calendar.SECOND, cTime.get(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, cTime.get(Calendar.MILLISECOND));
		
		if (c.before(Calendar.getInstance())) {
			c.add(Calendar.DATE, 1);
		}
		
		Log.i("NewAlarm", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(c.getTime()));
		*/
		/*int hour = c.get(Calendar.HOUR_OF_DAY);
		int min = r.nextInt(29);
		c.set(Calendar.MINUTE, min);
		if (hour < 10) {
			// Today at 10
			c.set(Calendar.HOUR_OF_DAY, 10);
		} else if (hour < 20) {
			// Today next hour
			c.add(Calendar.HOUR_OF_DAY, 1);
		} else {
			// Tomorrow at 10:XX
			c.set(Calendar.HOUR_OF_DAY, 10);
			c.roll(Calendar.DATE, true);
		}*/
		new ClassEvents(TAG, "INFO", "Setting up an alarm at " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(lSchedule)));
		Log.i("ScheduleMoodZoom", "Scheduling alarm: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(lSchedule)));
		a.set(AlarmManager.RTC_WAKEUP, lSchedule, pending);
	}
	
	public static void CreateBPTempSchedule(Context context, SharedPreferences prefs) {
		boolean bBPTemp_Enabled = prefs.getBoolean("checkboxHighIntensity", false) && 
								prefs.getBoolean("checkboxHighIntensityBPTempReminder", false) && 
								(prefs.getBoolean("checkboxHighIntensityBP", false) || 
								 prefs.getBoolean("checkboxHighIntensityTemp", false));
		
		long[] lScheduleTime = new long[3];
		lScheduleTime[0] = prefs.getLong("timeHighIntensityBPTempMorning", 25200000);
		lScheduleTime[1] = prefs.getLong("timeHighIntensityBPTempAfternoon", 61200000);
		lScheduleTime[2] = prefs.getLong("timeHighIntensityBPTempEvening", 79200000);
		
		SharedPreferences schedule = context.getSharedPreferences(
		        ClassConsts.BP_TEMP_SCHEDULE, Context.MODE_PRIVATE);
		
		Calendar dNow = Calendar.getInstance();
		Calendar cNow = Calendar.getInstance();
		cNow.clear();
		cNow.set(dNow.get(Calendar.YEAR), dNow.get(Calendar.MONTH), dNow.get(Calendar.DAY_OF_MONTH));
		
		
		SharedPreferences.Editor editor = schedule.edit();
		
		
		if (bBPTemp_Enabled) {
			editor.putBoolean("boolBPTempEnabled", true);
			
			Calendar[] cScheduleTime = new Calendar[3];
			
			for (int i = 0; i < 3; i++) {
				cScheduleTime[i] = Calendar.getInstance();
				cScheduleTime[i].setTimeInMillis(lScheduleTime[i]);
			}
			
			Calendar cLastSchedule = CombineDateTime(cNow, cScheduleTime[2]);
			
			if (!cLastSchedule.after(dNow)) {
				cNow.add(Calendar.DATE, 1);
			}
			
			for (int i = 0; i < 3; i++) {
				Calendar cSchedule = CombineDateTime(cNow, cScheduleTime[i]);
				editor.putLong("dateSchedule" + Integer.toString(i + 1), cSchedule.getTimeInMillis());
				Log.i("CreateBPTempSchedule", "Added schedule: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cSchedule.getTime()));
			}
			
		} else {
			editor.putBoolean("boolBPTempEnabled", false);
		}
		
		editor.putLong("dateSchedule", cNow.getTimeInMillis());
		
		editor.commit();
	}
	
	public static void ScheduleBPTemp(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		SharedPreferences schedule = context.getSharedPreferences(
		        ClassConsts.BP_TEMP_SCHEDULE, Context.MODE_PRIVATE);
		
		boolean bBPTemp_Enabled = schedule.getBoolean("boolBPTempEnabled", false);
		
		// Clear existing alarms
		new ClassEvents(TAG, "INFO", "Stopping alarm");
		AlarmManager a = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, ReceiverBPTemp.class);
		PendingIntent pending = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE);
		if (pending != null) {
			a.cancel(pending);
		}

		long lSchedule = 0;
		
		if (bBPTemp_Enabled) {
			Calendar cSchedule = Calendar.getInstance();
			for (int i = 0; i < 3; i++) {
				lSchedule = schedule.getLong("dateSchedule" + Integer.toString(i + 1), 0);
				cSchedule.setTimeInMillis(lSchedule);
				
				if (cSchedule.after(Calendar.getInstance())) {
					break;
				}
				lSchedule = 0;
			}
			
			if (lSchedule == 0) {
				Log.i("ScheduleBPTemp", "No valid schedule found. Rescheduling.");
				CreateBPTempSchedule(context, prefs);
				ScheduleBPTemp(context);
				return;
			}
			
			pending = PendingIntent.getBroadcast(context, 0, intent, 0);
			
			new ClassEvents(TAG, "INFO", "Setting up an alarm at " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(lSchedule)));
			Log.i("ScheduleBPTemp", "Scheduling alarm: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(lSchedule)));
			a.set(AlarmManager.RTC_WAKEUP, lSchedule, pending);
		}
	}
}