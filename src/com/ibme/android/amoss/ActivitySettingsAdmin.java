// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.ibme.android.oxlith.R;

import android.app.AlertDialog;
import android.content.DialogInterface.OnClickListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.widget.Toast;

public class ActivitySettingsAdmin extends PreferenceActivityWithMenu implements OnSharedPreferenceChangeListener {

	private static final String TAG = "AMoSSSettingsAdmin";

	public ActivitySettingsAdmin() {
		super(R.id.menu_settings_admin);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Preference pref;
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences_admin);

		pref = findPreference("editLocalStorage");
		pref.setOnPreferenceChangeListener(numberCheckListener);
		String local = ((EditTextPreference)pref).getText();
		pref.setSummary(local + " (15 days recommended, one day takes 1-5MB of external storage)");

		/* Disabled server and TC upload
		pref = findPreference("editServerRaw");
		pref.setSummary(((EditTextPreference)pref).getText());
		pref.setEnabled(false);

		pref = findPreference("editServerTC");
		pref.setSummary(((EditTextPreference)pref).getText());
		pref.setEnabled(false);
		*/

		pref = findPreference("checkboxHighIntensity");
		pref.setOnPreferenceChangeListener(confirmHighIntensityListener);
		
		/* Disabled server upload
		pref = findPreference("checkboxWifi");
		pref.setEnabled(false);
		*/
		
		// may increase data amount by 20x, so disable it for now
		//pref = findPreference("listSamplingRate");
		//pref.setSummary(((ListPreference)pref).getEntry());
		//pref.setEnabled(false);

		/* Disabled server upload
		pref = (Preference)findPreference("buttonCheck");
		pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference arg0) {
				new ClassEvents(TAG, "INFO", "Checking server connection");
				new TaskCheckerIBME(getApplicationContext()).execute();
			    return true;
			}
		});
		*/
		
		/* Disabled TC upload
		pref = (Preference)findPreference("buttonTcCheck");
		pref.setEnabled(false);
		*/
		/*pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference arg0) {
				new ClassEvents(TAG, "INFO", "Checking server connection");
				new TaskCheckerIBME(getApplicationContext()).execute();
			    return true;
			}
		});*/
		
		pref = (Preference)findPreference("pulseOxMACAddress");
		pref.setSummary(((ListPreference)pref).getValue());
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		UpdatePulseOx(prefs);
		
		/* Disabled server upload
		pref = (Preference)findPreference("buttonProteusUpload");
		pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference arg0) {
				new ClassEvents(TAG, "INFO", "Uploading Proteus data");
				
				TaskZipper zipper = new TaskZipper();
				zipper.setOutputDir(new File(Environment.getExternalStorageDirectory().toString() + ClassConsts.FILES_ROOT));
				zipper.setFilenameAppend(DateFormat.format(ClassConsts.PROTEUS_PATCH_DATA_DATE_FORMAT, System.currentTimeMillis()).toString());
				zipper.setStatusUpdatePreference(getApplicationContext(), "buttonProteusUpload");
				zipper.setUploadOnCompletion(getApplicationContext(), true);
				zipper.setDeleteOnCompletion(false);
				
				zipper.execute(new File(Environment.getExternalStorageDirectory().toString() + ClassConsts.PROTEUS_PATCH_DATA_File));
				
			    return true;
			}
		});
		
		pref.setSummary(prefs.getString("buttonProteusUpload", ""));
		
		// Custom configuration for OxLith
		pref.setEnabled(false);
		*/
		
		// Custom configuration for OxLith
		pref = findPreference("checkboxHighIntensity");
		pref.setEnabled(false);
	}

	private void UpdatePulseOx(SharedPreferences sharedPreferences) {
		boolean bValueHI = sharedPreferences.getBoolean("checkboxHighIntensity", false);
		boolean bValueHIPO = sharedPreferences.getBoolean("checkboxHighIntensityPulseOx", false);
		
		findPreference("checkboxHighIntensityPulseOx").setEnabled(bValueHI);
		findPreference("pulseOxMACAddress").setEnabled(bValueHI && bValueHIPO);
	}
	
	Preference.OnPreferenceChangeListener confirmHighIntensityListener = new Preference.OnPreferenceChangeListener() {
	    @Override
	    public boolean onPreferenceChange(Preference preference, Object newValue) {
			AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySettingsAdmin.this);
			String strTitle;
			String strMessage;
			if ((Boolean)newValue) {
				strTitle = ActivitySettingsAdmin.this.getResources().getString(R.string.pref_high_intensity_confirm_enabled_title);
				strMessage = ActivitySettingsAdmin.this.getResources().getString(R.string.pref_high_intensity_confirm_enabled_message);
			} else {
				strTitle = ActivitySettingsAdmin.this.getResources().getString(R.string.pref_high_intensity_confirm_disabled_title);
				strMessage = ActivitySettingsAdmin.this.getResources().getString(R.string.pref_high_intensity_confirm_disabled_message);
			}
			builder.setTitle(strTitle);
			builder.setMessage(strMessage);
			if ((Boolean)newValue) {
				builder.setPositiveButton(ActivitySettingsAdmin.this.getResources().getString(android.R.string.ok), enableHighIntensity);
			} else {
				builder.setPositiveButton(ActivitySettingsAdmin.this.getResources().getString(android.R.string.ok), disableHighIntensity);	
			}
			builder.setNegativeButton(ActivitySettingsAdmin.this.getResources().getString(android.R.string.cancel), null);
			AlertDialog dialog = builder.create();
			dialog.show();
			
			return false;
	    }
	    
	    private OnClickListener enableHighIntensity = new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ActivitySettingsAdmin.this);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean("checkboxHighIntensity", true);
				editor.apply();
				//editor.commit();
				
				long dateHighIntensityActivated = System.currentTimeMillis();
				
				prefs = ActivitySettingsAdmin.this.getSharedPreferences(
				        ClassConsts.PREFS_PRIVATE, Context.MODE_PRIVATE);
				editor = prefs.edit();
				editor.putLong("dateHighIntensityActivated", dateHighIntensityActivated);
				editor.commit();
				
				SimpleDateFormat fmt = new SimpleDateFormat(ClassConsts.DATEFMT);
				
				new ClassReporting("setting_high_intensity_activated_date", fmt.format(new Date(dateHighIntensityActivated)), ActivitySettingsAdmin.this, true);
			}
		};
		
	    private OnClickListener disableHighIntensity = new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ActivitySettingsAdmin.this);
				SharedPreferences.Editor editor = prefs.edit();
				editor.putBoolean("checkboxHighIntensity", false);
				editor.apply();
				//editor.commit();
				
				SimpleDateFormat fmt = new SimpleDateFormat(ClassConsts.DATEFMT);
				
				new ClassReporting("setting_high_intensity_deactivated_date", fmt.format(new Date(System.currentTimeMillis())), ActivitySettingsAdmin.this, true);
			}
		};
	};
	
	Preference.OnPreferenceChangeListener numberCheckListener = new Preference.OnPreferenceChangeListener() {
	    @Override
	    public boolean onPreferenceChange(Preference preference, Object newValue) {
			//Check that the string is an integer.
	        return numberCheck(newValue);
	    }
	};

	private boolean numberCheck(Object newValue) {
	    if( !newValue.toString().equals("")  &&  newValue.toString().matches("\\d*") ) {
	    	int i = Integer.valueOf(newValue.toString());
	    	if (i > 0) {
	    		return true;
	    	} else {
		        Toast.makeText(this, "Value should be > 0", Toast.LENGTH_SHORT).show();
		        return false;
	    	}
	    }
	    else {
	        Toast.makeText(this, newValue + " is not a number", Toast.LENGTH_SHORT).show();
	        return false;
	    }
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Preference pref = findPreference(key);
		if (key.equals("editUserBday") || key.equals("editUserLocation")) {
			pref.setSummary(((EditTextPreference)pref).getText());

		} else if (key.equals("editLocalStorage")) {
			String local = ((EditTextPreference)pref).getText();
			pref.setSummary(local + " (15 days recommended, one day takes 1-5MB of external storage)");

		} else if (key.equals("listUserGender") ||
				key.equals("listUserStatus") ||
				key.equals("listSamplingRate")) {
			pref.setSummary(((ListPreference)pref).getEntry());
		} else if (key.equals("checkboxHighIntensity")) {
			((CheckBoxPreference)pref).setChecked(sharedPreferences.getBoolean(key, false));
			UpdatePulseOx(sharedPreferences);
		} else if (key.equals("checkboxHighIntensityPulseOx")) {
			UpdatePulseOx(sharedPreferences);
		} else if (key.equals("pulseOxMACAddress")) {
			pref.setSummary(((ListPreference)pref).getValue());
		} else if (key.equals("buttonProteusUpload")) {
			pref.setSummary(sharedPreferences.getString(key, ""));
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}
}
