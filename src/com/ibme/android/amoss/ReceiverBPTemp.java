// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.android.amoss;

import com.ibme.android.oxlith.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class ReceiverBPTemp extends BroadcastReceiver {

	private static final String TAG = "ReceiverBPTemp";

	@Override
	public void onReceive(Context context, Intent intent) {
		// Generate signal and start activity
		new ClassEvents(TAG, "INFO", "Alarm");

		SharedPreferences schedule = context.getSharedPreferences(
		        ClassConsts.BP_TEMP_SCHEDULE, Context.MODE_PRIVATE);
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		
		boolean bBPTemp_Enabled = schedule.getBoolean("boolBPTempEnabled", false);
		
		Intent i = new Intent(context, ActivityBPTemp.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.putExtra("boolBPTempEnabled", bBPTemp_Enabled);
		context.startActivity(i);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		ServiceCollect.CreateBPTempSchedule(context, prefs);
		ServiceCollect.ScheduleBPTemp(context);
		
		if (bBPTemp_Enabled) {
			Resources res = context.getResources();
			
			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
			
			mBuilder.setSmallIcon(R.drawable.ic_notification_bar);
				mBuilder.setContentTitle(res.getString(R.string.high_intensity_bp_temp_notification_title));
				mBuilder.setContentText(res.getString(R.string.high_intensity_bp_temp_notification_message));
			
			String strUri = settings.getString("listNotificationSound", "");
			
			if (strUri == "") {
				mBuilder.setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);
			} else {
				mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
				
				mBuilder.setSound(Uri.parse(strUri));
			}
			
			// The stack builder object will contain an artificial back stack for the
			// started Activity.
			// This ensures that navigating backward from the Activity leads out of
			// your application to the Home screen.
			TaskStackBuilder stackBuilder = TaskStackBuilder.from(context);
			// Adds the back stack for the Intent (but not the Intent itself)
			stackBuilder.addParentStack(ActivityBPTemp.class);
			// Adds the Intent that starts the Activity to the top of the stack
			stackBuilder.addNextIntent(i);
			
			PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
			
			mBuilder.setContentIntent(resultPendingIntent);
			
			NotificationManager mNotificationManager =
			    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			
			mNotificationManager.notify(ClassConsts.NOTIFICATION_BP_TEMP, mBuilder.getNotification());
		}
	}
}
