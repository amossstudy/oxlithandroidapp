// Copyright (c) 2015, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.android.amoss;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

public class ReceiverLocationProviders extends BroadcastReceiver {

	private static final String TAG = "ReceiverPower";

	@Override
	public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
        	logProviderState(context);
        }
	}
	
	public static void logProviderState(Context context) {
    	try {
    		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				new ClassReporting("location_gps", "enabled", context, false);
				new ClassEvents(TAG, "INFO", "GPS STATE ENABLED");
            } else {
				new ClassReporting("location_gps", "disabled", context, false);
				new ClassEvents(TAG, "INFO", "GPS STATE DISABLED");
            }
            
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				new ClassReporting("location_network_provider", "enabled", context, false);
				new ClassEvents(TAG, "INFO", "LOCATION NETWORK_PROVIDER ENABLED");
            } else {
				new ClassReporting("location_network_provider", "disabled", context, false);
				new ClassEvents(TAG, "INFO", "LOCATION NETWORK_PROVIDER DISABLED");
            }
    	} catch (Exception e) {
    		new ClassEvents(TAG, "ERROR", "Could not retrieve location provider states " + e.getMessage());
    	}
	}
}
