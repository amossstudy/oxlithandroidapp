// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)
//          Arvind Raghu (arvind.raghu@eng.ox.ac.uk)

// Originally written by Arvind Raghu

package com.ibme.android.amoss;

/* class: MainActivity. java
 * This is a demonstration of BP acquistion through the A&D medical device.
 * 
 * Copyright (C) 2011 Arvind Raghu, IBME, University of Oxford, UK
 * Last Revised: 5th Feb, 2014
 * 
 */
/*
 * This program is licensed under the GNU General Public License, version 3 and is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY. You may contact the author by e-mail (airwind266@gmail.com). 
 */
import java.io.IOException;
import java.util.Calendar;

import com.ibme.android.oxlith.R;
import com.ibme.android.andmanager.ANDBloodPressure;
import com.ibme.android.andmanager.ANDHeader;
import com.ibme.android.andmanager.ANDManager;
import com.ibme.android.andmanager.ANDPacketParser;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;

public class ActivityBPTemp extends ActivityWithMenu implements OnSharedPreferenceChangeListener {

	private static final String TAG = "AMoSSBloodPressureTemperature";
	
	private EditText[] vEditText_SBP = new EditText[3]; //1, vEditText_SBP2, vEditText_SBP3;
	private EditText[] vEditText_DBP = new EditText[3]; //1, vEditText_DBP2, vEditText_DBP3;
	private EditText[] vEditText_HR = new EditText[3]; //1, vEditText_HR2, vEditText_HR3;
	private EditText[] vEditText_Temp = new EditText[3]; //1, vEditText_Temp2, vEditText_Temp3;
	private ImageView[] bpDrawing = new ImageView[3]; //1, bpDrawing2, bpDrawing3;
	private ProgressBar[] BPprogressBar = new ProgressBar[3]; //_1, BPprogressBar_2, BPprogressBar_3;
	private LinearLayout l1;
	private Button[] getBPbutton = new Button[3]; //_1, getBPbutton_2, getBPbutton_3;
	private Button buttonSubmit, buttonEnableBT;
	private TextView[] textBPStatus = new TextView[3]; //_1, textBPStatus_2, textBPStatus_3;
	private TextView[] textBPResult = new TextView[3]; //_1, textBPStatus_2, textBPStatus_3;
	private TextView labelBTStatus;
	private ImageView imageBTStatus, imageBTWarning;
	private View layoutBP, layoutTemp;
	private boolean skipValidate = false;
	
	private int iBPAcquisitionNo = -1;
	
	// constants for A&D BP device
	private ANDManager AND_Manager;
	public int MINUMUM_INTER_MEASURE_TIME = 5 * 60; // AT LEAST 4 MINUTES AGO
	
	boolean bBPEnabled, bTempEnabled;
	
	private ClassBloodPressure mBloodPressure;
	private ClassTemperature mTemperature;

	public ActivityBPTemp() {
		super(R.id.menu_bp_temp);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bp_temp);
		
		// Start services
		Intent service = new Intent(this, ServiceCollect.class);
		this.startService(service);
		service = new Intent(this, ServiceUpload.class);
		this.startService(service);
		
		final boolean bBPTemp_Enabled = getIntent().getBooleanExtra("boolBPTempEnabled", false);
		
		initialiseViews();
		button_BP_Listeners();
		dataBP();
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		
		settings.registerOnSharedPreferenceChangeListener(this);
		
		bBPEnabled = settings.getBoolean("checkboxHighIntensityBP", false);
		bTempEnabled = settings.getBoolean("checkboxHighIntensityTemp", false);
		
		if (bBPEnabled) {
			mBloodPressure = new ClassBloodPressure(this);
			if (!bTempEnabled) {
				vEditText_HR[2].setNextFocusDownId(View.NO_ID);
			}
				
		} else {
			layoutBP.setVisibility(View.GONE);
		}
		
		if (bTempEnabled) {
			mTemperature = new ClassTemperature(this);
		} else {
			layoutTemp.setVisibility(View.GONE);
		}
		
		if ((!bBPEnabled) && (!bTempEnabled)) {
			buttonSubmit.setText(R.string.button_text_close);
		}
		
		skipValidate = true;
		if (getCurrentFocus() != null) {
			getCurrentFocus().clearFocus();
		}
		skipValidate = false;
		
		buttonSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				long ts = System.currentTimeMillis();
				boolean bValid = true;
            	String str_sbp_1 = "", str_sbp_2 = "", str_sbp_3 = "";
            	String str_dbp_1 = "", str_dbp_2 = "", str_dbp_3 = "";
            	String str_hr_1 = "", str_hr_2 = "", str_hr_3 = "";
     			String str_temp_1 = "", str_temp_2 = "", str_temp_3 = "";
       		
     			// Don't perform validation on loose focus.
     			skipValidate = true;
     			
     			if ((bBPEnabled) && (bTempEnabled)
     					&& (!ValidateBPEntered(0)) && (!ValidateBPEntered(1)) && (!ValidateBPEntered(2))
     					&& (!ValidateTempEntered(0)) && (!ValidateTempEntered(1)) && (!ValidateTempEntered(2))) {
        			
            		makeToastwithID(R.string.notification_bp_temp_nothing_entered);
            		
     				bValid = false;
     			} else if ((bBPEnabled) && (!bTempEnabled)
     					&& (!ValidateBPEntered(0)) && (!ValidateBPEntered(1)) && (!ValidateBPEntered(2))) {

            		makeToastwithID(R.string.notification_bp_nothing_entered);
            		
     				bValid = false;
     			} else if ((!bBPEnabled) && (bTempEnabled)
     					&& (!ValidateTempEntered(0)) && (!ValidateTempEntered(1)) && (!ValidateTempEntered(2))) {

            		makeToastwithID(R.string.notification_temp_nothing_entered);
            		
     				bValid = false;
     			} else {
	        		if (bBPEnabled) {
		            	
	        			if (ValidateBPEntered(0)) {
			            	if (bValid) { if (ValidateSBP(0)) { str_sbp_1 = vEditText_SBP[0].getText().toString(); } else { vEditText_SBP[0].requestFocus(); bValid = false; } }
			            	if (bValid) { if (ValidateDBP(0)) { str_dbp_1 = vEditText_DBP[0].getText().toString(); } else { vEditText_DBP[0].requestFocus(); bValid = false; } }
			            	if (bValid) { if (ValidateHR(0))  { str_hr_1  = vEditText_HR[0].getText().toString();  } else { vEditText_HR[0].requestFocus();  bValid = false; } }
	        			}
	        			
	        			if (ValidateBPEntered(1)) {
				            if (bValid) { if (ValidateSBP(1)) { str_sbp_2 = vEditText_SBP[1].getText().toString(); } else { vEditText_SBP[1].requestFocus(); bValid = false; } }
			            	if (bValid) { if (ValidateDBP(1)) { str_dbp_2 = vEditText_DBP[1].getText().toString(); } else { vEditText_DBP[1].requestFocus(); bValid = false; } }
			            	if (bValid) { if (ValidateHR(1))  { str_hr_2  = vEditText_HR[1].getText().toString();  } else { vEditText_HR[1].requestFocus();  bValid = false; } }
	        			}
	        			
		            	if (ValidateBPEntered(2)) {
				            if (bValid) { if (ValidateSBP(2)) { str_sbp_3 = vEditText_SBP[2].getText().toString(); } else { vEditText_SBP[2].requestFocus(); bValid = false; } }
			            	if (bValid) { if (ValidateDBP(2)) { str_dbp_3 = vEditText_DBP[2].getText().toString(); } else { vEditText_DBP[2].requestFocus(); bValid = false; } }
			            	if (bValid) { if (ValidateHR(2))  { str_hr_3  = vEditText_HR[2].getText().toString();  } else { vEditText_HR[2].requestFocus();  bValid = false; } }
		            	}
	        		}
	        		
	        		if (bTempEnabled) {
	        			
	        			if (ValidateTempEntered(0)) {
					        if (bValid) { if (ValidateTemp(0)) { str_temp_1 = vEditText_Temp[0].getText().toString(); } else { vEditText_Temp[0].requestFocus(); bValid = false; } }
		        		}
	        			if (ValidateTempEntered(1)) {
	    					if (bValid) { if (ValidateTemp(1)) { str_temp_2 = vEditText_Temp[1].getText().toString(); } else { vEditText_Temp[1].requestFocus(); bValid = false; } }
	        			}
	        			if (ValidateTempEntered(2)) {
	    					if (bValid) { if (ValidateTemp(2)) { str_temp_3 = vEditText_Temp[2].getText().toString(); } else { vEditText_Temp[2].requestFocus(); bValid = false; } }
	        			}
	        		}
     			}
        		
        		skipValidate = false;
        		
        		if (bValid) {
        			
        			if (bBPEnabled) {
        				mBloodPressure.init(ts);
	            		mBloodPressure.update(ts, str_sbp_1, str_dbp_1, str_hr_1, str_sbp_2, str_dbp_2, str_hr_2, str_sbp_3, str_dbp_3, str_hr_3);
        			}
        			
        			if (bTempEnabled) {
    	            	mTemperature.init(ts);
    	            	mTemperature.update(ts, str_temp_1, str_temp_2, str_temp_3);
        			}
        			
	        		if ((bBPEnabled) || (bTempEnabled)) {
	        			Toast toast = Toast.makeText(getBaseContext(), R.string.notification_bp_temp_submitted, Toast.LENGTH_SHORT);
	            		toast.show();
	        		}
	            	
	            	if (bBPTemp_Enabled) {
		    			NotificationManager mNotificationManager =
		    				    (NotificationManager) ActivityBPTemp.this.getSystemService(Context.NOTIFICATION_SERVICE);
		    			
		    			mNotificationManager.cancel(ClassConsts.NOTIFICATION_BP_TEMP);
	            	}
	            	
	            	finish();
        		}
			}
		});
		
	    // Register for broadcasts on BluetoothAdapter state change
	    IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
	    this.registerReceiver(mReceiver, filter);
	    
		BluetoothAdapter mBA;
		mBA = BluetoothAdapter.getDefaultAdapter();

		if (mBA != null) {
			setBluetoothState(mBA.isEnabled());
		} else {
			setBluetoothState(false);
		}
	}
	
	@Override
	public void onDestroy() {
	    // Unregister broadcast listeners
	    this.unregisterReceiver(mReceiver);
	    
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
		prefs.unregisterOnSharedPreferenceChangeListener(this);
		
		if (mBloodPressure != null) {
			mBloodPressure.fini();
		}
		if (mTemperature != null) {
			mTemperature.fini();
		}
		new ClassEvents(TAG, "INFO", "Destroyed");
		super.onDestroy();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Log.i("onPrepareOptionsMenu", "onPrepareOptionsMenu");
		if ((AND_Manager != null) && (AND_Manager.isAlive())) {
			makeToastwithID(R.string.BP_complete_or_cancel);
			return false;
		} else {
			return super.onPrepareOptionsMenu(menu);
		}
	}
	
	@Override
	public void onBackPressed() {
		if ((AND_Manager != null) && (AND_Manager.isAlive())) {
			try {
				endANDThread();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			super.onBackPressed();
		}
	}
	
	private void initialiseViews() {
		
		l1 = (LinearLayout) findViewById(R.id.linearlayout1);
		layoutBP = findViewById(R.id.layoutBPContainer);
		layoutTemp = findViewById(R.id.layoutTempContainer);
		getBPbutton[0] = (Button) findViewById(R.id.button_acquire1);
		getBPbutton[1] = (Button) findViewById(R.id.button_acquire2);
		getBPbutton[2] = (Button) findViewById(R.id.button_acquire3);
		buttonSubmit = (Button) findViewById(R.id.buttonSubmit);
		buttonEnableBT = (Button) findViewById(R.id.buttonEnableBluetooth);
		labelBTStatus = (TextView) findViewById(R.id.labelBluetoothStatus);
		imageBTStatus = (ImageView) findViewById(R.id.imageBluetoothStatus);
		imageBTWarning = (ImageView) findViewById(R.id.imageBluetoothWarning);
		vEditText_SBP[0] = (EditText) findViewById(R.id.EditText_SBP1);
		vEditText_DBP[0] = (EditText) findViewById(R.id.EditText_DBP1);
		vEditText_HR[0] = (EditText) findViewById(R.id.EditText_HR1);
		vEditText_SBP[1] = (EditText) findViewById(R.id.EditText_SBP2);
		vEditText_DBP[1] = (EditText) findViewById(R.id.EditText_DBP2);
		vEditText_HR[1] = (EditText) findViewById(R.id.EditText_HR2);
		vEditText_SBP[2] = (EditText) findViewById(R.id.EditText_SBP3);
		vEditText_DBP[2] = (EditText) findViewById(R.id.EditText_DBP3);
		vEditText_HR[2] = (EditText) findViewById(R.id.EditText_HR3);
		bpDrawing[0] = (ImageView) findViewById(R.id.BP_drawing1);
		bpDrawing[1] = (ImageView) findViewById(R.id.BP_drawing2);
		bpDrawing[2] = (ImageView) findViewById(R.id.BP_drawing3);
		textBPStatus[0] = (TextView) findViewById(R.id.text_bp_status_1);
		textBPStatus[1] = (TextView) findViewById(R.id.text_bp_status_2);
		textBPStatus[2] = (TextView) findViewById(R.id.text_bp_status_3);
		textBPResult[0] = (TextView) findViewById(R.id.text_bp_result_1);
		textBPResult[1] = (TextView) findViewById(R.id.text_bp_result_2);
		textBPResult[2] = (TextView) findViewById(R.id.text_bp_result_3);
		BPprogressBar[0] = (ProgressBar) findViewById(R.id.progressBar_BP1);
		BPprogressBar[1] = (ProgressBar) findViewById(R.id.progressBar_BP2);
		BPprogressBar[2] = (ProgressBar) findViewById(R.id.progressBar_BP3);
		BPprogressBar[0].setVisibility(View.GONE);
		BPprogressBar[1].setVisibility(View.GONE);
		BPprogressBar[2].setVisibility(View.GONE);
		vEditText_Temp[0] = (EditText) findViewById(R.id.EditText_temp_1);
		vEditText_Temp[1] = (EditText) findViewById(R.id.EditText_temp_2);
		vEditText_Temp[2] = (EditText) findViewById(R.id.EditText_temp_3);
		
		buttonEnableBT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Both these ways of opening the bluetooth settings requires the BLUETOOTH_ADMIN permission.
				/*Intent intentOpenBluetoothSettings = new Intent();
				intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS); 
				startActivity(intentOpenBluetoothSettings);*/
				
				/*final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.bluetooth.BluetoothSettings");
                intent.setComponent(cn);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity( intent);*/
			}
		});
		
	}
	
	private void setBluetoothState(boolean state) {
		if (state) {
			labelBTStatus.setText(R.string.Bluetooth_enabled);
			imageBTStatus.setImageResource(R.drawable.ic_bluetooth_on);
			imageBTWarning.setImageResource(R.drawable.ic_ok);
			
			if (iBPAcquisitionNo == -1) {
				for (int j = 0; j < 3; j ++) {
					getBPbutton[j].setEnabled(true);
				}
			} else {
				getBPbutton[iBPAcquisitionNo].setEnabled(true);
			}
		} else {
			labelBTStatus.setText(R.string.Bluetooth_disabled);
			imageBTStatus.setImageResource(R.drawable.ic_bluetooth_off);
			imageBTWarning.setImageResource(R.drawable.ic_warning);
			
			for (int j = 0; j < 3; j ++) {
				if (j != iBPAcquisitionNo) {
					getBPbutton[j].setEnabled(false);
				}
			}
		}
		
		if (iBPAcquisitionNo == -1) {
		} else {

		}
	}
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        final String action = intent.getAction();

	        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
	            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
	                                                 BluetoothAdapter.ERROR);
	            switch (state) {
	            case BluetoothAdapter.STATE_OFF:
	            	setBluetoothState(false);
	                break;
	            case BluetoothAdapter.STATE_TURNING_OFF:
	            	setBluetoothState(false);
	                break;
	            case BluetoothAdapter.STATE_ON:
	            	setBluetoothState(true);
	                break;
	            case BluetoothAdapter.STATE_TURNING_ON:
	                break;
	            }
	        }
	    }
	};
	
	private void BPAcquire(int i) {
		if (iBPAcquisitionNo == i) {
			// Cancel acquisition
			
			try {
				endANDThread();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (iBPAcquisitionNo == -1) {
			// Start acquisition
			bpDrawing[i].setVisibility(View.GONE);
			textBPResult[i].setVisibility(View.GONE);
			getBPbutton[i].setText(R.string.BP_cancel);

			BPprogressBar[i].setVisibility(View.VISIBLE);
			BPprogressBar[i].setPressed(true);
			
			textBPStatus[i].setText(R.string.Status_BP_starting);
			
			for (int j = 0; j < 3; j ++) {
				if (j != i) {
					getBPbutton[j].setEnabled(false);
				}
			}
			
			buttonSubmit.setEnabled(false);
			
			/*
			 * BPdevice_status_tl.setBackgroundDrawable(getResources()
			 * .getDrawable(R.drawable.orange_tl));
			 */l1.requestLayout();

			Log.i("A&D", "BP_" + (i + 1) + " BT acquisition STARTED");

			checkStartANDThread();
			iBPAcquisitionNo = i;
		} else {
			//TODO: Another acquisition running
		}
	}
	
	private void button_BP_Listeners() {

		getBPbutton[0].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				BPAcquire(0);
			}
		});

		getBPbutton[1].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				BPAcquire(1);
			}
		});

		getBPbutton[2].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				BPAcquire(2);
			}
		});
	}
	
	
	// -----------------------------------------------------------A&D---------------------------------
		// A&D BP device code

		/**
		 * Start the AND Thread accordingly
		 */
		public void checkStartANDThread() {
			
			//enableBluetooth();
			makeDiscoverable();
			
			int portScale = -1;//readPortFromFile(); // pref.getInt(
												// "ANDScaleportNumber", -1);

			if (AND_Manager == null || !AND_Manager.isAlive()) {
				AND_Manager = new ANDManager(new ANDPacketParser(
						MINUMUM_INTER_MEASURE_TIME), messageHandler, portScale);
				AND_Manager.start();
			}
		}
		
		/*public boolean enableBluetooth() {
			boolean enabled = false;
			
			BluetoothAdapter mBA;
			mBA = BluetoothAdapter.getDefaultAdapter();

			if (mBA != null) {
				
				int attempts = 5;
				enabled = mBA.isEnabled();
				
				while (attempts >0 && !enabled) {
					
					mBA.enable();
					attempts--;
					
					try {
						Thread.sleep(400);
					} catch (InterruptedException e) {
						// ignore
					}
					enabled = mBA.isEnabled();
				}
			}
			return enabled;
		}*/
		
		private void makeDiscoverable() {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 3000);
			startActivity(discoverableIntent);
		}

		/**
		 * END the AND Thread accordingly
		 * 
		 * @throws IOException
		 */
		public void endANDThread() throws IOException {
			if ((AND_Manager != null) && (AND_Manager.isAlive())) {
				AND_Manager.turnOff();
			}
		}

		/**
		 * Handles the data coming from the BPMonitor
		 */
		Handler messageHandler = new Handler() {

			@Override
			public void handleMessage(Message m) {

				switch (m.what) {

				case ANDManager.TypeOfMessage.Listening:
					
					textBPStatus[iBPAcquisitionNo].setText(R.string.Status_BP_waiting);
					
					l1.requestLayout();
					
					break;
					
				case ANDManager.TypeOfMessage.Connected:
					
					textBPStatus[iBPAcquisitionNo].setText(R.string.Status_BP_connected);
					
					l1.requestLayout();
					
					break;
					
				case ANDManager.TypeOfMessage.DataBP:
					
					parseBPanswer(m);
					
					/*
					 * Note: The protocol here states that any measurement from A&D
					 * within the past 10 minutes (MINIMUM_INTER_MEASURE_TIME) is
					 * valid. If we are to change it, to say that every measure
					 * after the press of the respective ´Acquire´ button should
					 * hold, then get the time stamp of the press of the acquire
					 * button, and use it for filtering the readings. Remember that
					 * A&D sends untransmitted, stored readings in the order in
					 * which they were taken (i.e Oldest first). This means you will
					 * have to wait for the most recent reading, until the old ones
					 * have been transmitted.
					 */
					// Only in case of successful connection we have to store the
					// port number
					/*if (AND_Manager != null) {
						int portNum = AND_Manager.getPort();
						writeANDPort(portNum);
					}*/

					break;

				case ANDManager.TypeOfMessage.ConnectionUnsuccesfull:
					// TODO WE SHOULD NOTIFY THE ERROR
					break;
				
				case ANDManager.TypeOfMessage.ThreadFinished:
					
					getBPbutton[iBPAcquisitionNo].setText(R.string.Button_start);

					BPprogressBar[iBPAcquisitionNo].setVisibility(View.GONE);
					
					textBPStatus[iBPAcquisitionNo].setText(R.string.Status_BP_start);
					
					/*for (int j = 0; j < 3; j ++) {
						if (j != iBPAcquisitionNo) {
							getBPbutton[j].setEnabled(true);
						}
					}*/
					
					iBPAcquisitionNo = -1;
					
					BluetoothAdapter mBA;
					mBA = BluetoothAdapter.getDefaultAdapter();

					if (mBA != null) {
						setBluetoothState(mBA.isEnabled());
					} else {
						setBluetoothState(false);
					}
					
					buttonSubmit.setEnabled(true);
					
					l1.requestLayout();
					
					break;

				default:
					break;
				}
			}
		};

		/**
		 * @return the local timestamp in seconds
		 */
		public long getLocalTime() {
			// return globalTimestamp.getTimestamp();
			/*
			 * Time now = new Time(); now.setToNow(); Calendar c =
			 * Calendar.getInstance();
			 * 
			 * return Long.valueOf(now.toString());
			 */
			Calendar c = Calendar.getInstance();
			return c.getTimeInMillis() / 1000;
		}

		protected void parseBPanswer(Message m) {
			ANDBloodPressure bp = (ANDBloodPressure) m.obj;
			
		    int systolic = bp._SysDia;
			int diastolic = bp._Dia;
			int pulse = bp._Pulse;
			int msg = bp.getResponseMessage();
			
			ANDHeader headBP = bp._header;

			/* Compare measurement to device time using getMeasTransOffset() instead of
			 * comparing to phone time. 
			Calendar calBP = Calendar.getInstance();

			calBP.set(headBP._YearOfMeas, headBP._MonthOfMeas - 1,
					headBP._DayOfMeas, headBP._HourOfMeas,
					headBP._MinOfMeas, headBP._SecOfMeas);

			long recordingTSBP = calBP.getTimeInMillis() / 1000;

			long sessionTSBP = getLocalTime();*/

			if (Math.abs(headBP.getMeasTransOffset()) < MINUMUM_INTER_MEASURE_TIME) {
				// The measure is less than 5 minutes ago (We can use it)
				
				String text = systolic + " / " + diastolic + " (" + pulse + ")";
				makeToastwithText(text);
				
				if (iBPAcquisitionNo != -1) {
					vEditText_SBP[iBPAcquisitionNo].setText(String.valueOf(systolic));
					vEditText_DBP[iBPAcquisitionNo].setText(String.valueOf(diastolic));
					vEditText_HR[iBPAcquisitionNo].setText(String.valueOf(pulse));
					BPprogressBar[iBPAcquisitionNo].setVisibility(View.GONE);
					
					String strResult = "";
					String strLowBattery = "";
					
					if (bp.lowBattery()) {
						bpDrawing[iBPAcquisitionNo].setImageResource(R.drawable.stat_sys_battery_10);
						bpDrawing[iBPAcquisitionNo].setVisibility(View.VISIBLE);
						
						strLowBattery = getResources().getString(R.string.Error_BP_Low_Battery);
					} else if (bp.errorMeasurement()) {
						bpDrawing[iBPAcquisitionNo].setImageResource(R.drawable.ic_error);
						bpDrawing[iBPAcquisitionNo].setVisibility(View.VISIBLE);
					} else if (bp.warningMeasurement()) {
						bpDrawing[iBPAcquisitionNo].setImageResource(R.drawable.ic_warning);
						bpDrawing[iBPAcquisitionNo].setVisibility(View.VISIBLE);
					} else if (bp.correctMeasurement()) {
						bpDrawing[iBPAcquisitionNo].setImageResource(R.drawable.ic_ok);
						bpDrawing[iBPAcquisitionNo].setVisibility(View.VISIBLE);
					}
					
			    	switch (msg) {
			    	case ANDBloodPressure.ResponseMessage.CorrectMeasurement:
						if (strLowBattery.length() == 0) {
							strResult = getResources().getString(R.string.Error_BP_Correct);
							
							getBPbutton[iBPAcquisitionNo].setVisibility(View.GONE);
							textBPStatus[iBPAcquisitionNo].setVisibility(View.GONE);
						}
						break;
			    	case ANDBloodPressure.ResponseMessage.MeasurementError:
			    		strResult = getResources().getString(R.string.Error_BP_Measurement_Error);
			    		break;
			    	case ANDBloodPressure.ResponseMessage.CuffError:
			    		strResult = getResources().getString(R.string.Error_BP_Cuff_Error);
			    		break;
			    	case ANDBloodPressure.ResponseMessage.IHB:
			    		strResult = getResources().getString(R.string.Error_BP_IHB);
			    		break;
			    	case ANDBloodPressure.ResponseMessage.Movement:
			    		strResult = getResources().getString(R.string.Error_BP_Movement);
			    		break;
			    	case ANDBloodPressure.ResponseMessage.MovementIHB:
			    		strResult = getResources().getString(R.string.Error_BP_Movement_IHB);
			    		break;
			    	default:
			    		strResult = getResources().getString(R.string.Error_BP_Undefined);
			    		break;
			    	}

			    	if (strLowBattery.length() > 0) {
			    		if (strResult.length() == 0) {
			    			strResult = strLowBattery;
			    		} else {
			    			strResult = strResult + "\n" + strLowBattery;
			    		}
			    			
			    	}
					
			    	if (strResult.length() > 0) {
			    		textBPResult[iBPAcquisitionNo].setVisibility(View.VISIBLE);
			    		textBPResult[iBPAcquisitionNo].setText(strResult);
			    	}
			    	
					//bpDrawing1.setBackgroundDrawable(getResources().getDrawable(
					//		R.drawable.greentfl_normal));
					l1.invalidate();
					//iBPAcquisitionNo = -1;
				}
				try {
					endANDThread();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		protected void makeToastwithText(String toast_text) {
			Context context = getApplicationContext();
			CharSequence text = toast_text;
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
		}
		protected void makeToastwithID(int resId) {
			Context context = getApplicationContext();
			int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(context, resId, duration);
			toast.show();
		}
		// -------------------END of A&D device
		// code---------------------------------------------------------------------
		
		private boolean ValidateBPEntered(int i) {
			boolean bEntered = false;
			if (vEditText_SBP[i].getText().toString().length() > 0) {
        		bEntered = true;
        	}
			if (vEditText_DBP[i].getText().toString().length() > 0) {
        		bEntered = true;
        	}
			if (vEditText_HR[i].getText().toString().length() > 0) {
        		bEntered = true;
        	}
			return bEntered;
		}
		
		private boolean ValidateTempEntered(int i) {
			boolean bEntered = false;
			if (vEditText_Temp[i].getText().toString().length() > 0) {
        		bEntered = true;
        	}
			return bEntered;
		}
		
		private boolean ValidateSBP(int i) {
			return ValidateSBP(i, true, false);
		}
		
		private boolean ValidateSBP(int i, boolean bShowErrors, boolean bAllowBlank) {
			boolean bValid = true;
        	String s = vEditText_SBP[i].getText().toString();
            //Validate the string entered in EditText
        	if (s.equals("") || s.length() == 0) {
        		if (!bAllowBlank) {
	        		if (bShowErrors) {
	        			makeToastwithID(R.string.notification_bp_temp_incorrect_values);
	        		}
					bValid = false;
        		}
			}
			else {
				String g = s.toString();
				int gint = Integer.valueOf(g);
				if (gint < 50 || gint > 300) {
					if (bShowErrors) {
						makeToastwithID(R.string.notification_bp_temp_incorrect_values);
					}
					bValid = false;
				} else if (gint > 50 && gint < 300) {
					//nextStepButton.setEnabled(true);
				}
			}
			return bValid;
		}
		
		private boolean ValidateDBP(int i) {
			return ValidateDBP(i, true, false);
		}
		
		private boolean ValidateDBP(int i, boolean bShowErrors, boolean bAllowBlank) {
			boolean bValid = true;
        	String s = vEditText_DBP[i].getText().toString();
            //Validate the string entered in EditText
        	if (s.equals("") || s.length() == 0) {
        		if (!bAllowBlank) {
	        		if (bShowErrors) {
	        			makeToastwithID(R.string.notification_bp_temp_incorrect_values);
	        		}
					bValid = false;
        		}
			} else {
				String g = s.toString();
				int gint = Integer.valueOf(g);
				if (gint < 20 || gint > 150) {
					if (bShowErrors) {
						makeToastwithID(R.string.notification_bp_temp_incorrect_values);
					}
					bValid = false;
				} else if (gint > 20 && gint < 150) {
					//nextStepButton.setEnabled(true);
				}
			}
			return bValid;
		}
		
		private boolean ValidateHR(int i) {
			return ValidateHR(i, true, false);
		}
		
		private boolean ValidateHR(int i, boolean bShowErrors, boolean bAllowBlank) {
			boolean bValid = true;
        	String s = vEditText_HR[i].getText().toString();
            //Validate the string entered in EditText
        	if (s.equals("") || s.length() == 0) {
        		if (!bAllowBlank) {
	        		if (bShowErrors) {
	        			makeToastwithID(R.string.notification_bp_temp_incorrect_values);
	        		}
					bValid = false;
        		}
			} else {
				String g = s.toString();
				int gint = Integer.valueOf(g);
				if (gint < 10 || gint > 350) {
					if (bShowErrors) {
						makeToastwithID(R.string.notification_bp_temp_incorrect_values);
					}
					bValid = false;
				} else if (gint > 10 && gint < 350) {
					//nextStepButton.setEnabled(true);
				}
			}
			return bValid;
		}
		
		private boolean ValidateTemp(int i) {
			return ValidateTemp(i, true, false);
		}
		
		private boolean ValidateTemp(int i, boolean bShowErrors, boolean bAllowBlank) {
			boolean bValid = true;
        	String s = vEditText_Temp[i].getText().toString();
            //Validate the string entered in EditText
        	if (s.equals("") || s.length() == 0) {
        		if (!bAllowBlank) {
	        		if (bShowErrors) {
	        			makeToastwithID(R.string.notification_bp_temp_incorrect_values);
	        		}
					bValid = false;
        		}
			} else {
				String g = s.toString();
				float gint = Float.valueOf(g);
				if (!((gint >= 30 && gint <= 45) || (gint >= 86 && gint <= 113))) {
					if (bShowErrors) {
						makeToastwithID(R.string.notification_bp_temp_incorrect_values);
					}
					bValid = false;
				} else if ((gint >= 30 && gint <= 45) || (gint >= 86 && gint <= 113)) {
					//nextStepButton.setEnabled(true);
				}
			}
			return bValid;
		}
		
		private void dataBP() {
			
			vEditText_SBP[0].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		            if (!hasFocus && !skipValidate) {
		            	ValidateSBP(0);
		    		}
		        }
		        
		    });
			vEditText_SBP[1].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateSBP(1);
		    		}
		        }
		        
		    });
			vEditText_SBP[2].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateSBP(2);
		    		}
		        }
		        
		    });
			vEditText_DBP[0].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateDBP(0);
		    		}
		        }
		        
		    });
			vEditText_DBP[1].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateDBP(1);
		    		}
		        }
		        
		    });
			vEditText_DBP[2].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateDBP(2);
		    		}
		        }
		        
		    });
			
			vEditText_HR[0].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateHR(0);
		    		}
		        }
		        
		    });
			
			vEditText_HR[1].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateHR(1);
		    		}
		        }
		        
		    });
			
			vEditText_HR[2].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateHR(2);
		    		}
		        }
		        
		    });
		
			vEditText_Temp[0].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateTemp(0);
		    		}
		        }
		        
		    });
		
			vEditText_Temp[1].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateTemp(1);
		    		}
		        }
		        
		    });
		
			vEditText_Temp[2].setOnFocusChangeListener(new OnFocusChangeListener() { 
		        @Override
		        public void onFocusChange(View v, boolean hasFocus) {
		        	if (!hasFocus && !skipValidate) {
		            	ValidateTemp(2);
		    		}
		        }
		        
		    });
		}

		@Override
		public void onSharedPreferenceChanged(SharedPreferences settings, String key) {
			Log.i("onSharedPreferenceChanged", "onSharedPreferenceChanged BP Temp");
			if (key.equals("checkboxHighIntensityBP") || key.equals("checkboxHighIntensityTemp")) {
				
				bBPEnabled = settings.getBoolean("checkboxHighIntensityBP", false);
				bTempEnabled = settings.getBoolean("checkboxHighIntensityTemp", false);
				
				if (bBPEnabled) {
					if (mBloodPressure == null) {
						mBloodPressure = new ClassBloodPressure(this);
					}
					layoutBP.setVisibility(View.VISIBLE);
					if (!bTempEnabled) {
						vEditText_HR[2].setNextFocusDownId(View.NO_ID);
					} else {
						vEditText_HR[2].setNextFocusDownId(vEditText_Temp[0].getId());
					}
				} else {
					layoutBP.setVisibility(View.GONE);
				}
				
				if (bTempEnabled) {
					if (mTemperature == null) {
						mTemperature = new ClassTemperature(this);
					}
					layoutTemp.setVisibility(View.VISIBLE);
				} else {
					layoutTemp.setVisibility(View.GONE);
				}
				
				if ((!bBPEnabled) && (!bTempEnabled)) {
					buttonSubmit.setText(R.string.button_text_close);
				} else {
					buttonSubmit.setText(R.string.mood_zoom_text_submit);
				}
			}
			l1.requestLayout();
		}
}
