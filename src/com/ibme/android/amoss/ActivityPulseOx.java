// Original version Copyright (c) 2013, J. Behar, A. Roebuck, M. Shahid,
// J. Daly, A. Hallack, N. Palmius, K. Niehaus, G. Clifford (University
// of Oxford). All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// NOT MEDICAL SOFTWARE.
// 
// This software is provided for informational or research purposes only,
// and is not for professional medical use, diagnosis, treatment or care,
// nor is it intended to be a substitute therefor. Always seek the advice
// of a physician or other qualified health provider properly licensed to
// practice medicine or general healthcare in your jurisdiction concerning
// any questions you may have regarding any health problem. Never disregard
// professional medical advice or delay in seeking it because of something
// you have observed through the use of this software. Always consult with
// your physician or other qualified health care provider before embarking
// on a new treatment, diet or fitness programme.
//
// Graphical charts copyright (c) AndroidPlot (http://androidplot.com/)
// All rights reserved.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by J. Behar, A. Roebuck, M. Shahid, J. Daly, A. Hallack,
// N. Palmius, K. Niehaus, G. Clifford

package com.ibme.android.amoss;

import com.ibme.android.oxlith.R;
import com.ibme.android.noninmanager.NoninConsts;
import com.ibme.android.noninmanager.NoninManager;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.Plot;
import com.androidplot.series.XYSeries;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.SimpleXYSeries.ArrayFormat;
import com.androidplot.xy.XYPlot;

public class ActivityPulseOx extends Activity {
	
	private static final String TAG = "AMoSSSignalsRecorder";

	private File ppgFile, spo2File, rawFile, rawindexFile;
	private String dateTimeString;
	private String filesDirPath;
	private NotificationManager notificationManager;
	private UserInterfaceUpdater graphUpdateTask;
	private SharedPreferences sharedPreferences;
	private WakeLock wakeLock;
	private BluetoothAdapter bluetoothAdapter;
	private NoninManager noninManager;
	private Button startButton, stopButton, submitButton;
	private ImageView recordingSign;
	private boolean screenLocked;
	private boolean recordingStarted;
	private boolean batteryLow;
	private boolean currentlyConnected;
	private TextView labelBTStatus;
	private ImageView imageBTStatus, imageBTWarning;
	private ProgressBar progressBar;
	private TextView textPulseOxStatus;

	private BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// Stop recording if battery level is less than 5%.
			int batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
			int batteryScale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
			float percentage = (float) batteryLevel / (float) batteryScale;
			
			if (percentage < ClassConsts.BATTERY_NOTIFICATION_THRESHOLD) {
				batteryLow = true;
				if (recordingStarted) {
					stopRecording();
				} else {
					startButton.setEnabled(false);
					startButton.setClickable(false);
					
					recordingSign.setVisibility(View.GONE);
					progressBar.setVisibility(View.GONE);
					textPulseOxStatus.setText(R.string.Status_pulse_ox_battery_low);
				}
			}
		}
	};

	private void setBluetoothState(boolean state) {
		if (state) {
			labelBTStatus.setText(R.string.Bluetooth_enabled);
			imageBTStatus.setImageResource(R.drawable.ic_bluetooth_on);
			imageBTWarning.setImageResource(R.drawable.ic_ok);
			textPulseOxStatus.setText(R.string.Status_pulse_ox_start);
		} else {
			labelBTStatus.setText(R.string.Bluetooth_disabled);
			imageBTStatus.setImageResource(R.drawable.ic_bluetooth_off);
			imageBTWarning.setImageResource(R.drawable.ic_warning);
			textPulseOxStatus.setText(R.string.Bluetooth_disabled);
		}
		
		startButton.setClickable(state);
		startButton.setEnabled(state);
		
		recordingSign.setVisibility(View.GONE);
		progressBar.setVisibility(View.GONE);
	}
	
	private BroadcastReceiver bluetoothDisconnectReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED) || action.equals(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)) {
				Toast.makeText(getApplicationContext(), R.string.bluetoothConnectionLost, Toast.LENGTH_LONG).show();
				currentlyConnected = false;
				if (!batteryLow) {
					startButton.setClickable(true);
					startButton.setEnabled(true);
				}
				if (noninManager != null && noninManager.isRunning()) {
					noninManager.prepareToStop();
				}
				recordingSign.setVisibility(View.GONE);
				progressBar.setVisibility(View.GONE);
				if ((!batteryLow) && (!recordingStarted)) {
					textPulseOxStatus.setText(R.string.Status_pulse_ox_start);
					startButton.setText(R.string.Button_pulse_ox_start);
				} else if (!batteryLow) {
					textPulseOxStatus.setText(R.string.Status_pulse_ox_reconnect);
					stopButton.setClickable(true);
					stopButton.setEnabled(true);
					startButton.setText(R.string.Button_pulse_ox_reconnect);
				} else {
					textPulseOxStatus.setText(R.string.Status_pulse_ox_battery_low);
					startButton.setText(R.string.Button_pulse_ox_start);
				}
				
				if (!recordingStarted) {
					submitButton.setClickable(true);
					submitButton.setEnabled(true);
				} else {
					handelerAttemptReconnect.sendEmptyMessageDelayed(0, ClassConsts.PULSE_OX_AUTO_RECONNECT * 1000);
				}
				
				if (batteryLow) {
					submitRecording();
				}
				return;
			}
			if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
				Toast.makeText(getApplicationContext(), R.string.bluetoothConnected, Toast.LENGTH_SHORT).show();

				return;
			}
			if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
				// Bluetooth has been turned on/off.
				int bluetoothState = BluetoothAdapter.getDefaultAdapter().getState();
				if (bluetoothState == BluetoothAdapter.STATE_ON) {
					//Toast.makeText(getApplicationContext(), R.string.bluetoothTurnedBackOn, Toast.LENGTH_LONG).show();
					currentlyConnected = false;
					
					setBluetoothState(true);
					
					if (noninManager != null && noninManager.isRunning()) {
						noninManager.prepareToStop();
					}
					
					if (recordingStarted) {
						textPulseOxStatus.setText(R.string.Status_pulse_ox_reconnect);
						stopButton.setClickable(true);
						stopButton.setEnabled(true);
						startButton.setText(R.string.Button_pulse_ox_reconnect);

						handelerAttemptReconnect.sendEmptyMessageDelayed(0, ClassConsts.PULSE_OX_AUTO_RECONNECT * 1000);
					}
						
					return;
				}
				if (bluetoothState == BluetoothAdapter.STATE_OFF) {
					//Toast.makeText(getApplicationContext(), R.string.bluetoothTurnedOff, Toast.LENGTH_LONG).show();
					currentlyConnected = false;
					
					handelerAttemptReconnect.removeMessages(0);
					
					setBluetoothState(false);

					if (noninManager != null && noninManager.isRunning()) {
						noninManager.prepareToStop();
					}
					return;
				}
			}
		}
	};

	private Handler handelerAttemptReconnect = new Handler() {
	    public void handleMessage(Message m) {
	    	if (recordingStarted) {
	    		startRecording();
	    	}
	    }
	};
	
	private static class PpgHandler extends Handler {
		private final WeakReference<ActivityPulseOx> weakReference;

		public PpgHandler(ActivityPulseOx signalsRecorder) {
			this.weakReference = new WeakReference<ActivityPulseOx>(signalsRecorder);
		}

		@Override
		public void handleMessage(Message message) {
			ActivityPulseOx activityReference = weakReference.get();
			switch (message.what) {
			case NoninConsts.CODE_BLUTOOTH_NO_PAIRED_DEVICES:
				Toast.makeText(activityReference.getApplicationContext(), R.string.noPairedDevices, Toast.LENGTH_LONG).show();
				activityReference.currentlyConnected = false;
				activityReference.recordingSign.setVisibility(View.GONE);
				activityReference.progressBar.setVisibility(View.GONE);
				activityReference.textPulseOxStatus.setText(R.string.Status_pulse_ox_start);
				activityReference.startButton.setText(R.string.Button_pulse_ox_start);
				break;
			case NoninConsts.CODE_BLUETOOTH_CONNECTION_UNSUCCESSFUL:
				Toast.makeText(activityReference.getApplicationContext(), R.string.bluetoothConnectionUnsuccessful,
						Toast.LENGTH_LONG).show();
				activityReference.currentlyConnected = false;
				if (!activityReference.batteryLow) {
					activityReference.startButton.setClickable(true);
					activityReference.startButton.setEnabled(true);
				}
				if (activityReference.noninManager != null && activityReference.noninManager.isRunning()) {
					activityReference.noninManager.prepareToStop();
				}
				activityReference.recordingSign.setVisibility(View.GONE);
				activityReference.progressBar.setVisibility(View.GONE);
				if ((!activityReference.batteryLow) && (!activityReference.recordingStarted)) {
					activityReference.textPulseOxStatus.setText(R.string.Status_pulse_ox_start);
					activityReference.startButton.setText(R.string.Button_pulse_ox_start);
				} else if (!activityReference.batteryLow) {
					activityReference.textPulseOxStatus.setText(R.string.Status_pulse_ox_reconnect);
					activityReference.stopButton.setClickable(true);
					activityReference.stopButton.setEnabled(true);
					activityReference.startButton.setText(R.string.Button_pulse_ox_reconnect);
				} else {
					activityReference.textPulseOxStatus.setText(R.string.Status_pulse_ox_battery_low);
					activityReference.startButton.setText(R.string.Button_pulse_ox_start);
				}
				
				if (!activityReference.recordingStarted) {
					activityReference.submitButton.setClickable(true);
					activityReference.submitButton.setEnabled(true);
				} else {
					activityReference.handelerAttemptReconnect.sendEmptyMessageDelayed(0, ClassConsts.PULSE_OX_AUTO_RECONNECT * 1000);
				}
				
				if (activityReference.batteryLow) {
					activityReference.submitRecording();
				}
				break;
			case NoninConsts.CODE_BLUETOOTH_CONNECTION_SUCCESSFUL:
				Toast.makeText(activityReference.getApplicationContext(), R.string.bluetoothConnected, Toast.LENGTH_SHORT).show();
				activityReference.currentlyConnected = true;
				activityReference.recordingSign.setVisibility(View.VISIBLE);
				activityReference.progressBar.setVisibility(View.GONE);
				activityReference.textPulseOxStatus.setText(R.string.Status_pulse_ox_connected);
				activityReference.stopButton.setEnabled(true);
				activityReference.stopButton.setClickable(true);
				activityReference.recordingStarted = true;
				activityReference.startButton.setText(R.string.Button_pulse_ox_start);
				break;
			case NoninConsts.CODE_BLUETOOTH_RAW_PACKET_RECEIVED:
				Bundle bRaw = message.getData();
				byte[] raw = bRaw.getByteArray("raw");
				
				try {
					long timestamp = System.currentTimeMillis();
					
					BufferedWriter osIndex = new BufferedWriter(new FileWriter(activityReference.rawindexFile, true));
					
					SimpleDateFormat fmt = new SimpleDateFormat(ClassConsts.DATEFMT);
					
					osIndex.append(fmt.format(new Date(timestamp)) + "," + String.valueOf(activityReference.rawFile.length() + 1) + "\n");
					
					osIndex.flush();
					osIndex.close();
					
					DataOutputStream os = new DataOutputStream(new FileOutputStream(activityReference.rawFile, true));
					os.writeChars("\n" + String.valueOf(timestamp) + "\n");
					os.write(raw);
					
					os.flush();
					os.close();
				} catch (IOException e) {
					Log.e(TAG, "Error writing raw data to file", e);
				}
				
				break;
			case NoninConsts.CODE_BLUETOOTH_PACKET_RECEIVED:
				// Write PPG and SpO2 data to file.
				Bundle bundle = message.getData();
				double spo2 = bundle.getDouble("spo2");
				double[] ppgWave = bundle.getDoubleArray("ppgArray");
				long bundleStartTime = System.currentTimeMillis();

				try {
					BufferedWriter ppgBufferedWriter = new BufferedWriter(new FileWriter(activityReference.ppgFile, true));
					double timestamp = bundleStartTime;
					// TODO: sort out these timestamps properly: the below
					// is a dirty hack. Maybe timestamp each individual
					// frame as it arrives?

					// It's the packet that's timestamped (i.e. we have here
					// 25 frames, and we're just getting a timestamp now).
					// So artificially create the other timestamps by adding
					// 1/75 seconds after each frame is written.
					for (double val : ppgWave) {
						ppgBufferedWriter.append(String.valueOf(Math.round(timestamp)) + ",");
						ppgBufferedWriter.append(String.valueOf(val) + "\n");
						timestamp += 13.33;
					}
					ppgBufferedWriter.flush();
					ppgBufferedWriter.close();
				} catch (IOException e) {
					Log.e(TAG, "Error writing PPG data to file", e);
				}
				try {
					BufferedWriter spo2BufferedWriter = new BufferedWriter(new FileWriter(activityReference.spo2File, true));
					spo2BufferedWriter.append(String.valueOf(bundleStartTime) + ",");
					spo2BufferedWriter.append(String.valueOf(spo2) + "\n");
					spo2BufferedWriter.flush();
					spo2BufferedWriter.close();
				} catch (IOException e) {
					Log.e(TAG, "Error writing SpO2 data to file", e);
				}
				break;
			}
		}
	};

	private class UserInterfaceUpdater extends AsyncTask<Void, Void, String> {
		private XYPlot _ppgPlot;
		private boolean _stopRunningFlag;
		private PointLabelFormatter _plf;
		private LineAndPointFormatter _ppgFormatter;
		private XYSeries _ppgSeries;
		
		@Override
		protected String doInBackground(Void... params) {
			_ppgPlot = (XYPlot) findViewById(R.id.ppgPlot);
			_plf = new PointLabelFormatter(getResources().getColor(R.color.transparent));
			_ppgFormatter = new LineAndPointFormatter(Color.RED, null, getResources().getColor(R.color.translucentRed), _plf);

			initialisePlot(_ppgPlot);

			_stopRunningFlag = false;
			while (!_stopRunningFlag) {
				try {
					Thread.sleep(ClassConsts.UI_UPDATE_PERIOD);
				} catch (InterruptedException e) {
					Log.e(TAG, "InterruptedException during UI thread sleep", e);
					continue;
				}

				publishProgress();
			}
			return "Stopped";
		}

		public void initialisePlot(XYPlot plot) {
			Paint whitePaint = new Paint();
			whitePaint.setColor(Color.WHITE);
			whitePaint.setAlpha(255);
			plot.getLayoutManager().remove(plot.getLegendWidget());
			plot.setBorderStyle(Plot.BorderStyle.SQUARE, null, null);
			plot.setBorderPaint(null);
			plot.getGraphWidget().getRangeLabelPaint().setColor(Color.WHITE);
			plot.getGraphWidget().getDomainLabelPaint().setColor(Color.WHITE);
			plot.getGraphWidget().getRangeOriginLabelPaint().setColor(Color.WHITE);
			plot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.WHITE);
			plot.getGraphWidget().getBackgroundPaint().setAlpha(0);
			plot.getGraphWidget().setGridBackgroundPaint(whitePaint);
			plot.setDomainLabel("");
			plot.setRangeLabel("");
			plot.setTitle("");
			plot.getGraphWidget().getRangeLabelPaint().setAlpha(0);
			plot.getGraphWidget().getDomainLabelPaint().setAlpha(0);
			plot.getGraphWidget().getDomainOriginLabelPaint().setAlpha(0);
			plot.getGraphWidget().getRangeOriginLabelPaint().setAlpha(0);
			plot.setPlotPadding(-60, -22, -5, -35); // L,T,R,B
			plot.setMarkupEnabled(false);
			plot.getGraphWidget().getGridLinePaint().setAlpha(0);
		}

		@Override
		protected void onProgressUpdate(Void... progress) {

			if (!screenLocked) {
				// Only bother updating the UI if the screen is currently
				// unlocked.
				try {
					// PPG.
					List<Number> ppgVals = doubleQueueToNumberList(noninManager.getPpgQueue());
					_ppgSeries = new SimpleXYSeries(ppgVals, ArrayFormat.Y_VALS_ONLY, "");
					_ppgPlot.removeSeries(_ppgSeries);
					_ppgPlot.clear();
					_ppgPlot.addSeries(_ppgSeries, _ppgFormatter);
					_ppgPlot.redraw();
				} catch (OutOfMemoryError e) {
					Log.e(TAG, "OutOfMemoryError during UI update", e);
					resetUi();
				}
			}
		}

		@Override
		protected void onPostExecute(String result) {
			if (result.equals("Stopped")) {
				// Thread was cancelled.
			}
		}

		public void stopUiUpdates() {
			_stopRunningFlag = true;
		}

		public boolean isRunning() {
			return !_stopRunningFlag;
		}
	}

	/**
	 * Called when the activity is first created. onStart() is called
	 * immediately afterwards.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Make activity full screen.
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.pulse_ox);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

		// TODO: Implement this again
		/*
		// Log both handled and unhandled issues.
		if (sharedPreferences.getBoolean(ClassConsts.PREF_WRITE_LOG, ClassConsts.DEFAULT_WRITE_LOG)) {
			String bugDirPath = Environment.getExternalStorageDirectory().toString() + "/" + getString(R.string.app_name) + "/"
					+ ClassConsts.FILENAME_LOG_DIRECTORY;
			File bugDir = new File(bugDirPath);
			if (!bugDir.exists()) {
				bugDir.mkdirs();
			}
			String handledFileName = bugDirPath + "/logcat" + System.currentTimeMillis() + ".trace";
			String unhandledFileName = bugDirPath + "/unhandled" + System.currentTimeMillis() + ".trace";
			// Log any warning or higher, and write it to handledFileName.
			String[] cmd = new String[] { "logcat", "-f", handledFileName, "*:W" };
			try {
				Runtime.getRuntime().exec(cmd);
			} catch (IOException e1) {
				Log.e(TAG, "Error creating bug files", e1);
			}
			Thread.setDefaultUncaughtExceptionHandler(new CustomExceptionHandler(unhandledFileName));
		}*/

		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		dateTimeString = DateFormat.format(ClassConsts.PULSE_OX_DIR_DATE_FORMAT, System.currentTimeMillis()).toString();
		String appDirPath = Environment.getExternalStorageDirectory().toString() + ClassConsts.FILES_ROOT + ClassConsts.PULSE_OX_SUB_DIR;
		filesDirPath = appDirPath + "/" + dateTimeString + "/";
		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		recordingSign = (ImageView) findViewById(R.id.recordingSign);

		if (new File(appDirPath).isDirectory()) {
			// Archive old pulse ox recordings
			ArrayList<File> toZip = new ArrayList<File>();
			File[] files = new File(appDirPath).listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					if (files[i].listFiles().length == 0) {
						if (files[i].delete()) {
							new ClassEvents(TAG, "INFO", "Deleted " + files[i].getPath());
						} else {
							new ClassEvents(TAG, "ERROR", "Delete failed " + files[i].getPath());
						}
					} else {
						toZip.add(files[i]);
					}
				}
			}
			
			if (toZip.size() > 0) {
				new TaskFolderZipper(new File(Environment.getExternalStorageDirectory().toString() + ClassConsts.FILES_ROOT)).execute(toZip.toArray(new File[toZip.size()]));
			}
		}
		
		// Battery check receiver.
		registerReceiver(this.batteryLevelReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

		// Button to stop the recording.
		stopButton = (Button) findViewById(R.id.buttonStop);
		stopButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View viewNext) {
				// Check user actually wanted to cancel...
				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ActivityPulseOx.this);
				dialogBuilder.setTitle(getString(R.string.backPressedAlertTitle));
				dialogBuilder.setMessage(getString(R.string.backPressedAlertMessage));
				dialogBuilder.setNegativeButton(getString(android.R.string.no), null);
				dialogBuilder.setPositiveButton(getString(android.R.string.yes), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						stopRecording();
					}
				});
				dialogBuilder.create().show();
			}
		});
		stopButton.setEnabled(false);
		stopButton.setClickable(false);

		// Button to reconnect the bluetooth.
		startButton = (Button) findViewById(R.id.buttonStart);
		startButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View viewNext) {
				handelerAttemptReconnect.removeMessages(0);
				startRecording();
			}
		});

		// Button to submit the recording.
		submitButton = (Button) findViewById(R.id.buttonSubmit);
		submitButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View viewNext) {
				submitRecording();
			}
		});
		
		recordingStarted = false;
		batteryLow = false;
		currentlyConnected = false;
		
		labelBTStatus = (TextView) findViewById(R.id.labelBluetoothStatus);
		imageBTStatus = (ImageView) findViewById(R.id.imageBluetoothStatus);
		imageBTWarning = (ImageView) findViewById(R.id.imageBluetoothWarning);
		textPulseOxStatus = (TextView) findViewById(R.id.text_pulse_ox_status);
		progressBar = (ProgressBar) findViewById(R.id.progressBar_connect);
		progressBar.setVisibility(View.GONE);
		
		// Create a folder for the recordings, and delete any extra recordings.
		File dir = new File(filesDirPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		// Create txt files.
		ppgFile = new File(filesDirPath, ClassConsts.FILENAME_PPG);
		spo2File = new File(filesDirPath, ClassConsts.FILENAME_SPO2);
		rawFile = new File(filesDirPath, ClassConsts.FILENAME_RAW);
		rawindexFile = new File(filesDirPath, ClassConsts.FILENAME_RAW_INDEX);
		
		/*// Start PPG recording.
		if (bluetoothAdapter != null) {
			String macAddress = sharedPreferences.getString(ClassConsts.PREF_PULSE_OX_MAC_ADDRESS, ClassConsts.DEFAULT_PULSE_OX_MAC_ADDRESS);
			noninManager = new NoninManager(this, bluetoothAdapter, macAddress, new PpgHandler(ActivityPulseOx.this));
			noninManager.start();
		}*/

		wakeLock.acquire();

		// Set up listener so that if Bluetooth connection is lost we set give
		// the user an option to reconnect.
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
		filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
		filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
		filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);

		registerReceiver(bluetoothDisconnectReceiver, filter);

		if (bluetoothAdapter != null) {
			setBluetoothState(bluetoothAdapter.isEnabled());
		} else {
			setBluetoothState(false);
		}
		
		// Start graphs update.
		UserInterfaceUpdater initgraph = new UserInterfaceUpdater();
		XYPlot ppgPlot = (XYPlot) findViewById(R.id.ppgPlot);
		initgraph.initialisePlot(ppgPlot);
		initgraph = null;
	}

	@Override
	protected void onStart() {
		screenLocked = false;
		super.onStart();
	}

	@Override
	protected void onStop() {
		screenLocked = true;
		super.onStop();
	}

	public void resetUi() {
		graphUpdateTask.stopUiUpdates();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			Log.e(TAG, "Error while thread sleeping", e);
		}
		System.gc();
		graphUpdateTask = new UserInterfaceUpdater();
		graphUpdateTask.execute();
	}

	protected void startRecording() {
		if (bluetoothAdapter != null) {
			String macAddress = sharedPreferences.getString(ClassConsts.PREF_PULSE_OX_MAC_ADDRESS, ClassConsts.DEFAULT_PULSE_OX_MAC_ADDRESS);
			noninManager = null;
			noninManager = new NoninManager(getApplicationContext(), bluetoothAdapter, macAddress, new PpgHandler(ActivityPulseOx.this));
			noninManager.start();
			startButton.setEnabled(false);
			startButton.setClickable(false);
			
			recordingSign.setVisibility(View.GONE);
			progressBar.setVisibility(View.VISIBLE);
			progressBar.setPressed(true);
			textPulseOxStatus.setText(R.string.Status_pulse_ox_connecting);
			
			// Notify user
			Intent notificationIntent = new Intent(ActivityPulseOx.this, ActivityPulseOx.class);
			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

			NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
			builder.setSmallIcon(R.drawable.ic_notification_bar_pulse_ox);
			//builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification));
			builder.setContentTitle(getString(R.string.high_intensity_pulse_ox_notification_title));
			builder.setContentText(getString(R.string.high_intensity_pulse_ox_notification_message));
			builder.setAutoCancel(false);
			builder.setContentIntent(pendingIntent);
			
			notificationManager.notify(ClassConsts.NOTIFICATION_PULSE_OX, builder.getNotification());
			
			// Start graphs update.
			if (graphUpdateTask != null) {
				graphUpdateTask.stopUiUpdates();
			}
			
			graphUpdateTask = new UserInterfaceUpdater();
			graphUpdateTask.execute();
			
			stopButton.setEnabled(false);
			stopButton.setClickable(false);
			
			submitButton.setEnabled(false);
			submitButton.setClickable(false);
		}
	}
	protected void stopRecording() {

		recordingStarted = false;
		
		// Stop graphs update.
		if ((graphUpdateTask != null) && (graphUpdateTask.isRunning())) {
			graphUpdateTask.stopUiUpdates();
		}

		// PPG
		if (noninManager != null) {
			noninManager.prepareToStop();
		}

		try {
			wakeLock.release();
		} catch (Throwable t) {
			Log.e(TAG, "Wakelock has already been released", t);
		}
		
		// Cancel recording notification.
		notificationManager.cancel(ClassConsts.NOTIFICATION_PULSE_OX);

		recordingSign.setVisibility(View.GONE);
		progressBar.setVisibility(View.GONE);

		if (currentlyConnected) {
			textPulseOxStatus.setText(R.string.Status_pulse_ox_stopping);
		} else {
			if (bluetoothAdapter != null) {
				setBluetoothState(bluetoothAdapter.isEnabled());
			} else {
				setBluetoothState(false);
			}
			
			if ((bluetoothAdapter != null) && (!bluetoothAdapter.isEnabled())) {
				setBluetoothState(false);
				startButton.setText(R.string.Button_pulse_ox_start);
			} else if (!batteryLow) {
				startButton.setClickable(true);
				startButton.setEnabled(true);
				textPulseOxStatus.setText(R.string.Status_pulse_ox_start);
				startButton.setText(R.string.Button_pulse_ox_start);
			} else {
				textPulseOxStatus.setText(R.string.Status_pulse_ox_battery_low);
				startButton.setText(R.string.Button_pulse_ox_start);
			}
			
			submitButton.setClickable(true);
			submitButton.setEnabled(true);
			
			if (batteryLow) {
				submitRecording();
			}
		}
		
		stopButton.setEnabled(false);
		stopButton.setClickable(false);
		
		// Let the user know the recording is over.
		/*AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		dialogBuilder.setTitle(getString(R.string.finishedAlertTitle));
		dialogBuilder.setMessage(getString(R.string.finishedAlertSuccess));
		
		dialogBuilder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		dialogBuilder.setCancelable(false);
		dialogBuilder.create().show();*/

		// TODO: Implement this again
		/*
		// Change logcat back if necessary.
		if (sharedPreferences.getBoolean(ClassConsts.PREF_WRITE_LOG, ClassConsts.DEFAULT_WRITE_LOG)) {
			String[] cmd = new String[] { "logcat", "-f", "stdout", "*:V" };
			try {
				Runtime.getRuntime().exec(cmd);
			} catch (IOException e) {
				Log.e(TAG, "Error changing logcat back to default", e);
			}
		}*/
	}

	protected void submitRecording() {

		// Unregister listeners.
		try {
			unregisterReceiver(bluetoothDisconnectReceiver);
		} catch (Exception e) {
			Log.e(TAG, "Error unregistering bluetooth disconnect BroadcastReceiver", e);
		}
		try {
			unregisterReceiver(batteryLevelReceiver);
		} catch (Exception e) {
			Log.e(TAG, "Error unregistering battery level BroadcastReceiver", e);
		}

		// Let the user know the recording is over.
		/*AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		dialogBuilder.setTitle(getString(R.string.finishedAlertTitle));
		dialogBuilder.setMessage(getString(R.string.finishedAlertSuccess));
		
		dialogBuilder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		dialogBuilder.setCancelable(false);
		dialogBuilder.create().show();*/
		File fRecordings = new File(filesDirPath);
		if (fRecordings.exists() && fRecordings.listFiles().length > 0) {
			new TaskFolderZipper(new File(Environment.getExternalStorageDirectory().toString() + ClassConsts.FILES_ROOT)).execute(fRecordings);
			
    		Toast.makeText(getBaseContext(), R.string.notification_pulse_ox_submitted, Toast.LENGTH_SHORT).show();
		} else if (fRecordings.exists() && fRecordings.listFiles().length == 0) {
			fRecordings.delete();
		}
		
		finish();

		// TODO: Implement this again
		/*
		// Change logcat back if necessary.
		if (sharedPreferences.getBoolean(ClassConsts.PREF_WRITE_LOG, ClassConsts.DEFAULT_WRITE_LOG)) {
			String[] cmd = new String[] { "logcat", "-f", "stdout", "*:V" };
			try {
				Runtime.getRuntime().exec(cmd);
			} catch (IOException e) {
				Log.e(TAG, "Error changing logcat back to default", e);
			}
		}*/
	}
	
	private List<Number> doubleQueueToNumberList(Queue<Double> queue) {
		List<Number> list;
		// If the Queue is modified while the loop is running (which is more
		// than possible), a ConcurrentModificationException will be thrown.
		// If one is, it is caught and we try again.
		int attempts = 0;
		while (attempts < 5) {
			try {
				list = new ArrayList<Number>();
				for (Iterator<Double> iter = queue.iterator(); iter.hasNext();) {
					Double obj = iter.next();
					list.add(obj);
				}
				return list;
			} catch (ConcurrentModificationException e) {
				// Don't return anything so we go through the while loop again.
				attempts++;
			}
		}
		list = new ArrayList<Number>();
		list.add(0);
		list.add(0);
		return list;
	}

	@Override
	public void onBackPressed() {
		if (recordingStarted) {
			// Check user actually wanted to cancel...
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
			dialogBuilder.setTitle(getString(R.string.backPressedAlertTitle));
			dialogBuilder.setMessage(getString(R.string.backPressedAlertMessage));
			dialogBuilder.setNegativeButton(getString(android.R.string.no), null);
			dialogBuilder.setPositiveButton(getString(android.R.string.yes), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					stopRecording();
				}
			});
			dialogBuilder.create().show();
		} else if (submitButton.isEnabled()) {
			submitRecording();
		} else {
			Toast.makeText(getApplicationContext(), R.string.backPressedNotAvailable, Toast.LENGTH_LONG).show();
		}
	}
}