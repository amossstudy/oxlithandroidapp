// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Originally written by Maxim Osipov

package com.ibme.android.amoss;

import com.ibme.android.oxlith.R;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityMoodZoom extends ActivityWithMenu implements OnSharedPreferenceChangeListener {

	private static final String TAG = "AMoSSMoodZoom";

	private ClassMoodZoom mMoodZoom;
	private ClassMoodZoomStress mMoodZoomStress;

	public ActivityMoodZoom() {
		super(R.id.menu_mood_zoom);
	}
	
//	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onCreate(Bundle savedInstanceState) {
//		if (BuildConfig.DEBUG) {
//			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//	                 .detectAll()
//	                 .penaltyLog()
//	                 .build());
//		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mood_zoom);

		// Start services
		Intent service = new Intent(this, ServiceCollect.class);
		this.startService(service);
		service = new Intent(this, ServiceUpload.class);
		this.startService(service);
		
		final long lMoodZoomType = getIntent().getLongExtra("typeMoodZoom", 0); // 0: disabled, 1: daily, 2: high intensity
		
		mMoodZoom = new ClassMoodZoom(this);
		mMoodZoomStress = new ClassMoodZoomStress(this);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		final boolean bHIMZ_Enabled = prefs.getBoolean("checkboxHighIntensity", false) && 
								      prefs.getBoolean("checkboxHighIntensityMoodZoom", false);
		
		final TextView textDescription = (TextView) findViewById(R.id.textViewDesc);
		final TextView textStressLevel = (TextView) findViewById(R.id.textStressLevelDesc);
		final TextView textStressCause = (TextView) findViewById(R.id.textStressCauseDesc);
		
		if (bHIMZ_Enabled) {
			textDescription.setText(R.string.mood_zoom_text_description_current);
			textStressLevel.setText(R.string.stress_level_current);
			textStressCause.setText(R.string.stress_cause_current);
		}
		
		final RadioGroup radioStressLevel = (RadioGroup) findViewById(R.id.radioGroupStressLevel);
		
		radioStressLevel.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (Integer.parseInt(((RadioButton) findViewById(checkedId)).getText().toString()) <= 1) {
					if (bHIMZ_Enabled) {
						textStressCause.setText(R.string.stress_cause_current_optional);
					} else {
						textStressCause.setText(R.string.stress_cause_today_optional);
					}
				} else {
					if (bHIMZ_Enabled) {
						textStressCause.setText(R.string.stress_cause_current);
					} else {
						textStressCause.setText(R.string.stress_cause_today);
					}
				}
			}
		});
		
		final EditText editStressInformation = (EditText) findViewById(R.id.editStressInformation);
		
		final CheckBox radioStress_1 = (CheckBox) findViewById(R.id.radioStressCause_1);
        final CheckBox radioStress_2 = (CheckBox) findViewById(R.id.radioStressCause_2);
        final CheckBox radioStress_3 = (CheckBox) findViewById(R.id.radioStressCause_3);
        final CheckBox radioStress_4 = (CheckBox) findViewById(R.id.radioStressCause_4);
        final CheckBox radioStress_5 = (CheckBox) findViewById(R.id.radioStressCause_5);
        final CheckBox radioStress_6 = (CheckBox) findViewById(R.id.radioStressCause_6);
        
        radioStress_1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				radioStress_2.setChecked(false);
				radioStress_3.setChecked(false);
				radioStress_4.setChecked(false);
				radioStress_5.setChecked(false);
				radioStress_6.setChecked(false);
			}
		});
        radioStress_2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				radioStress_1.setChecked(false);
				radioStress_3.setChecked(false);
				radioStress_4.setChecked(false);
				radioStress_5.setChecked(false);
				radioStress_6.setChecked(false);
			}
		});
        radioStress_3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				radioStress_1.setChecked(false);
				radioStress_2.setChecked(false);
				radioStress_4.setChecked(false);
				radioStress_5.setChecked(false);
				radioStress_6.setChecked(false);
			}
		});
        
        radioStress_4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				radioStress_1.setChecked(false);
				radioStress_2.setChecked(false);
				radioStress_3.setChecked(false);
				radioStress_5.setChecked(false);
				radioStress_6.setChecked(false);
			}
		});
        radioStress_5.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				radioStress_1.setChecked(false);
				radioStress_2.setChecked(false);
				radioStress_3.setChecked(false);
				radioStress_4.setChecked(false);
				radioStress_6.setChecked(false);
			}
		});
        radioStress_6.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				radioStress_1.setChecked(false);
				radioStress_2.setChecked(false);
				radioStress_3.setChecked(false);
				radioStress_4.setChecked(false);
				radioStress_5.setChecked(false);
			}
		});
        
		// Handle submit button
        final Button submit = (Button) findViewById(R.id.buttonSubmit);
        final RadioGroup radioMood_1 = (RadioGroup) findViewById(R.id.radioGroupMood_1);
        final RadioGroup radioMood_2 = (RadioGroup) findViewById(R.id.radioGroupMood_2);
        final RadioGroup radioMood_3 = (RadioGroup) findViewById(R.id.radioGroupMood_3);
        final RadioGroup radioMood_4 = (RadioGroup) findViewById(R.id.radioGroupMood_4);
        final RadioGroup radioMood_5 = (RadioGroup) findViewById(R.id.radioGroupMood_5);
        final RadioGroup radioMood_6 = (RadioGroup) findViewById(R.id.radioGroupMood_6);
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	int mood_1_id = radioMood_1.getCheckedRadioButtonId();
            	int mood_2_id = radioMood_2.getCheckedRadioButtonId();
            	int mood_3_id = radioMood_3.getCheckedRadioButtonId();
            	int mood_4_id = radioMood_4.getCheckedRadioButtonId();
            	int mood_5_id = radioMood_5.getCheckedRadioButtonId();
            	int mood_6_id = radioMood_6.getCheckedRadioButtonId();
            	int stress_level_id = radioStressLevel.getCheckedRadioButtonId();
            	
            	int stress_cause_id = -1;
            	
            	if (radioStress_1.isChecked()) {
            		stress_cause_id = radioStress_1.getId();
            	} else if (radioStress_2.isChecked()) {
            		stress_cause_id = radioStress_2.getId();
            	} else if (radioStress_3.isChecked()) {
            		stress_cause_id = radioStress_3.getId();
            	} else if (radioStress_4.isChecked()) {
            		stress_cause_id = radioStress_4.getId();
            	} else if (radioStress_5.isChecked()) {
            		stress_cause_id = radioStress_5.getId();
            	} else if (radioStress_6.isChecked()) {
            		stress_cause_id = radioStress_6.getId();
            	}

            	if ((mood_1_id == -1) || (mood_2_id == -1) || (mood_3_id == -1) ||
            			(mood_4_id == -1) || (mood_5_id == -1) || (mood_6_id == -1)) {
            		Toast.makeText(getBaseContext(), R.string.notification_mood_zoom_complete_all, Toast.LENGTH_SHORT).show();
            	} else if (stress_level_id == -1) {
            		Toast.makeText(getBaseContext(), R.string.notification_mood_zoom_complete_stress, Toast.LENGTH_SHORT).show();
            	} else if ((Integer.parseInt(((RadioButton) findViewById(stress_level_id)).getText().toString()) > 1) &&
            			(stress_cause_id == -1)) {
            		Toast.makeText(getBaseContext(), R.string.notification_mood_zoom_complete_stress_cause, Toast.LENGTH_SHORT).show();
                } else {
	            	String mood_1 = ((RadioButton) findViewById(mood_1_id)).getText().toString();
	            	String mood_2 = ((RadioButton) findViewById(mood_2_id)).getText().toString();
	            	String mood_3 = ((RadioButton) findViewById(mood_3_id)).getText().toString();
	            	String mood_4 = ((RadioButton) findViewById(mood_4_id)).getText().toString();
	            	String mood_5 = ((RadioButton) findViewById(mood_5_id)).getText().toString();
	            	String mood_6 = ((RadioButton) findViewById(mood_6_id)).getText().toString();
	            	String stress_level = ((RadioButton) findViewById(stress_level_id)).getText().toString();
	            	String stress_cause = "";
	            	
	            	if (stress_cause_id != -1) {
	            		stress_cause = ((CheckBox) findViewById(stress_cause_id)).getText().toString();
	            	}
	            	
	            	String stress_information = editStressInformation.getText().toString();
	            	
	        		long ts = System.currentTimeMillis();
	        		mMoodZoom.init(ts);
	            	mMoodZoom.update(getBaseContext(), ts, mood_1, mood_2, mood_3, mood_4, mood_5, mood_6);
	            	mMoodZoomStress.init(ts);
	            	mMoodZoomStress.update(getBaseContext(), ts, stress_level, stress_cause, stress_information);
	            	Toast toast = Toast.makeText(getBaseContext(), R.string.notification_mood_zoom_submitted, Toast.LENGTH_SHORT);
	            	toast.show();
	            	
	            	if (lMoodZoomType > 0) {
		    			NotificationManager mNotificationManager =
		    				    (NotificationManager) ActivityMoodZoom.this.getSystemService(Context.NOTIFICATION_SERVICE);
		    			
		    			mNotificationManager.cancel(ClassConsts.NOTIFICATION_MOOD_ZOOM);
	            	}
	            	
	            	finish();
            	}
            }
        });
    }

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
		prefs.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
		prefs.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onDestroy() {
		if (mMoodZoom != null) {
			mMoodZoom.fini();
		}
		if (mMoodZoomStress != null) {
			mMoodZoomStress.fini();
		}
		new ClassEvents(TAG, "INFO", "Destroyed");
		super.onDestroy();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
	}
}
