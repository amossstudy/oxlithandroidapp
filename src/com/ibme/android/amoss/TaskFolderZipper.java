// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry; 
import java.util.zip.ZipOutputStream; 

import android.os.AsyncTask;

public class TaskFolderZipper extends AsyncTask<File, Void, Void> {

	private static final String TAG = "AMoSSTaskFolderZipper";

	private String outDir;
	
	public TaskFolderZipper(File outputDir) {
		outDir = outputDir.getAbsolutePath() + "/";
	}
	
	public static void zipFolder(String outputDir, File... files)
	{
		try {
			// Compress files
			for (int i = 0; i < files.length; i++) {
				File inf = files[i];
				String infRel = inf.getAbsolutePath().replace(outputDir, "");
				
				File outf = new File(outputDir + infRel.replace('/', '-') + ".zip");
				ZipOutputStream outs = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outf)));

				File[] subfiles = inf.listFiles();
				
				for (int j = 0; j < subfiles.length; j++) {
					File subfile = subfiles[j];
					BufferedInputStream ins = new BufferedInputStream(new FileInputStream(subfile));
					// Compress data
					ZipEntry entry = new ZipEntry(subfile.getAbsolutePath().replace(inf.getAbsolutePath() + "/", ""));
					outs.putNextEntry(entry);
					byte data[] = new byte[ClassConsts.BUFFER_SIZE];
					int count;
					while ((count = ins.read(data, 0, ClassConsts.BUFFER_SIZE)) != -1) {
						outs.write(data, 0, count);
					}
					ins.close();
					subfile.delete();
				}
				outs.close();
				
				// Remove uncompressed folder
				inf.delete();
				new ClassEvents(TAG, "INFO", "Zipped " + outf.getName());
			}
		} catch (Exception e) {
			new ClassEvents(TAG, "ERROR", "Zipper failed " + e.getMessage());
		}
	}
	
	@Override
	protected Void doInBackground(File... files) {
		zipFolder(outDir, files);
		return null;
	}
}
