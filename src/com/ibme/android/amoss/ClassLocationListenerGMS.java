// Copyright (c) 2015, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.android.amoss;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class ClassLocationListenerGMS implements com.google.android.gms.location.LocationListener,
		GooglePlayServicesClient.ConnectionCallbacks,  GooglePlayServicesClient.OnConnectionFailedListener {

	private static final String TAG = "AMoSSLocationListenerGMS";

	private LocationClient mLocationClient;
	private LocationRequest mLocationRequest;
	
	private ClassLocation mLocation;
	
	private Context mContext;

	public ClassLocationListenerGMS(Context context) {
		mContext = context;
	}
	
	public void init(long ts) {
		int statusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
		
		new ClassReporting("google_play_services_available", Integer.toString(statusCode), mContext, false);
		
	    if (statusCode != ConnectionResult.SUCCESS) {
	        Log.e(TAG, "isGooglePlayServicesAvailable = " + Integer.toString(statusCode));
	    }
	    
		mLocation = new ClassLocation(mContext, "_gms");
		mLocation.init(ts);
		mLocationClient = new LocationClient(mContext,this,this);
		mLocationClient.connect();
	}
	
	public void fini() {
		if ((mLocationClient != null) && (mLocationClient.isConnected())) {
			mLocationClient.removeLocationUpdates(this);
			mLocationClient.disconnect();
		}
		if (mLocation != null) {
			mLocation.fini();
		}
	}
	
	@Override
	public void onLocationChanged(Location loc) {
		long ts = loc.getTime();
		double lat = loc.getLatitude();
		double lon = loc.getLongitude();
		if (mLocation != null) {
			mLocation.update(ts, lat, lon);
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		new ClassReporting("location_client_connection", String.format("failed: %d", arg0.getErrorCode()), mContext, false);
		Log.e(TAG, "onConnectionFailed");
	}

	@Override
	public void onConnected(Bundle arg0) {
		new ClassReporting("location_client_connection", "connected", mContext, false);
		Log.i(TAG, "onConnected");
		
		mLocationRequest = LocationRequest.create();
		// Request an update every 5 minutes
		mLocationRequest.setInterval(5 * 1000 * 60);
		// Request the most accurate locations available - more likely to
		// use GPS (if available).
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}

	@Override
	public void onDisconnected() {
		new ClassReporting("location_client_connection", "disconnected", mContext, false);
		Log.e(TAG, "onDisconnected");
	    new Handler().post(new Runnable() {
	        @Override
	        public void run() {
	        	if ((mLocationClient != null) && (mLocationClient.isConnected())) {
		            mLocationClient.removeLocationUpdates(ClassLocationListenerGMS.this);
		            mLocationClient.disconnect();
	        	}

	            mLocationClient = new LocationClient(mContext, ClassLocationListenerGMS.this, ClassLocationListenerGMS.this);
	            mLocationClient.connect(); // NOW WORKING
	        }
	    });
	}
}
