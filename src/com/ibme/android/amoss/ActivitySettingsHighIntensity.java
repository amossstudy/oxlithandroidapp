// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.android.amoss;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.ibme.android.oxlith.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;

public class ActivitySettingsHighIntensity extends PreferenceActivityWithMenu implements OnSharedPreferenceChangeListener {

	//private static final String TAG = "AMoSSSettingsHighIntensity";

	public ActivitySettingsHighIntensity() {
		super(R.id.menu_settings_high_intensity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Preference pref;
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences_high_intensity);

		pref = findPreference("labelHighIntensityEnabled");
		pref.setEnabled(false);
		
		SharedPreferences prefs = this.getSharedPreferences(
		        ClassConsts.PREFS_PRIVATE, Context.MODE_PRIVATE);
		
		long lHighIntensityActivated = prefs.getLong("dateHighIntensityActivated", 0);
		
		if (lHighIntensityActivated == 0) {
		} else {
			String strDisabledHighIntensity = getResources().getString(R.string.pref_high_intensity_enabled_summary);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.UK);
			pref.setSummary(strDisabledHighIntensity.replace("%TIME%", sdf.format(new Date(lHighIntensityActivated))));
		}
		
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		
		UpdateMoodZoomSchedule(prefs);
		UpdateBPTempSchedule(prefs);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Preference pref = findPreference(key);
		if (key.equals("checkboxHighIntensityMoodZoom")) {
			UpdateMoodZoomSchedule(sharedPreferences);
		} else if (key.equals("checkboxHighIntensityBP") || key.equals("checkboxHighIntensityTemp") ||
				key.equals("checkboxHighIntensityBPTempReminder")) {
			UpdateBPTempSchedule(sharedPreferences);
		}
	}
	private void UpdateMoodZoomSchedule(SharedPreferences sharedPreferences) {
		boolean bValue = sharedPreferences.getBoolean("checkboxHighIntensityMoodZoom", true);
		UpdateMoodZoomSchedule(bValue);		
	}
	private void UpdateMoodZoomSchedule(boolean bMoodZoomEnabled) {
		Preference pref = findPreference("scheduleHighIntensityMoodZoom");
		pref.setEnabled(bMoodZoomEnabled);
		
		String strSummary = getResources().getString(R.string.pref_high_intensity_mood_zoom_schedule_summary);
		String strDisabled = getResources().getString(R.string.pref_high_intensity_mood_zoom_summary_disabled);
		
		if (bMoodZoomEnabled) {
			pref.setSummary(strSummary);
		} else {
			pref.setSummary(strSummary + "\n" + strDisabled);
		}
	}

	private void UpdateBPTempSchedule(SharedPreferences sharedPreferences) {
		boolean bValueBP = sharedPreferences.getBoolean("checkboxHighIntensityBP", true);
		boolean bValueTemp = sharedPreferences.getBoolean("checkboxHighIntensityTemp", true);
		boolean bValueReminder = sharedPreferences.getBoolean("checkboxHighIntensityBPTempReminder", true);
		UpdateBPTempSchedule(bValueBP || bValueTemp, bValueReminder);
	}
	private void UpdateBPTempSchedule(boolean bBPTempEnabled, boolean bBPTempReminderEnabled) {
		Preference pref = findPreference("checkboxHighIntensityBPTempReminder");
		pref.setEnabled(bBPTempEnabled);
		
		pref = findPreference("scheduleHighIntensityBPTemp");
		pref.setEnabled(bBPTempEnabled && bBPTempReminderEnabled);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}
}
