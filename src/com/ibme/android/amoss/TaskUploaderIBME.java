// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;


public class TaskUploaderIBME extends AsyncTask<File, TaskUploaderIBME.UploaderProgress, File[]> {
	
	class UploaderProgress {
		private String file;
		private String status;
		
		public UploaderProgress(File file, String status) {
			this.file = file.getName();
			this.status = status;
		}
		
		@Override
		public String toString() {
			return "Uploading " + file + " " + status;
		}
	}
	
	private static final String TAG = "AMoSSTaskUploaderIBME";

	final Context context;
	private String mUserID;
	private String mUserPass;
	private String mServerRaw;
	private String mFilename;
	private String mMimeType;
	private String mStatusUpdatePreference;

	TaskUploaderIBME(Context context) {
		this.context = context;
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		mUserID = prefs.getString("editUserID", "");
		mUserPass = prefs.getString("editUserPass", "");
		mServerRaw = prefs.getString("editServerRaw", "");
		mFilename = "";
		mMimeType = "";
		mStatusUpdatePreference = "";
	}

	TaskUploaderIBME(Context context, String mimeType, String filename) {
		this(context);
		
		mFilename = filename;
		mMimeType = mimeType;
	}
	
	public void setStatusUpdatePreference(String preference) {
		mStatusUpdatePreference = preference;
	}
	
	/*public class MyHttpClient extends DefaultHttpClient {

		final Context context;

		public MyHttpClient(Context context) {
			this.context = context;
		}

		@Override
		protected ClientConnectionManager createClientConnectionManager() {
			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", newSslSocketFactory(), 443));
			return new SingleClientConnManager(getParams(), registry);
		}

		private SSLSocketFactory newSslSocketFactory() {
			try {
				KeyStore trusted = KeyStore.getInstance("BKS");
				InputStream in = context.getResources().openRawResource(R.raw.ibmeweb7);
				try {
					trusted.load(in, "ez24get".toCharArray());
				} finally {
					in.close();
				}
				return new SSLSocketFactory(trusted);
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		}
	}*/

	public File[] uploadFiles(File... files) {
		File inf = null;
		ArrayList<File> completed = new ArrayList<File>();
		
		Log.e(TAG, "Uploading");
		if (TextUtils.isEmpty(mUserID) || TextUtils.isEmpty(mUserPass) || TextUtils.isEmpty(mServerRaw)) {
			return null;
		}

		final String strUplaodPath = mServerRaw + ClassConsts.URL_UPLOAD;
		
		for (int i = 0; i < files.length; i ++) {
			try {
				inf = files[i];
				
				publishProgress(new UploaderProgress(inf, "started"));
				
				CredentialsProvider cp = new BasicCredentialsProvider();
				cp.setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
						new UsernamePasswordCredentials(mUserID, mUserPass));
				DefaultHttpClient httpclient = new DefaultHttpClient();
				httpclient.setCredentialsProvider(cp);
				HttpPost httppost = new HttpPost(strUplaodPath);

				// Prepare HTTP request
				MultipartEntity entity = new MultipartEntity();
				entity.addPart( "MAX_FILE_SIZE", new StringBody(ClassConsts.UPLOAD_SIZE));
				entity.addPart( "USER", new StringBody(mUserID));
				entity.addPart( "PASS", new StringBody(mUserPass));

				FileBody fb;
				if (mFilename.length() > 0) {
					fb = new FileBody(inf, mFilename, mMimeType, null);
				} else {
					fb = new FileBody(inf);
				}
				entity.addPart( "FILE", fb);

				httppost.setEntity( entity );

				// Execute HTTP request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity rsp = response.getEntity();
				if (rsp != null) {
					String str = EntityUtils.toString(rsp);
					if (str.matches("^OK(?s).*")) {
						inf.delete();
						new ClassEvents(TAG, "INFO", "Uploaded " + files[i].getName());
						publishProgress(new UploaderProgress(inf, "finished"));
						completed.add(inf);
					} else {
						new ClassEvents(TAG, "ERROR", "Upload IBME error: " + str);
						publishProgress(new UploaderProgress(inf, "failed with error: " + str));
					}
				}
			} catch (Exception e) {
				new ClassEvents(TAG, "ERROR", "Upload IBME failed " + e.getMessage());
				publishProgress(new UploaderProgress(inf, "failed: " + e.getMessage()));
			}		
		}
		return completed.toArray(new File[completed.size()]);
	}
	
	@Override
	protected File[] doInBackground(File... files) {
		return uploadFiles(files);
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		if ((mStatusUpdatePreference.length() > 0) && (context != null)) {
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString(mStatusUpdatePreference, "Waiting to upload file(s)");
			editor.commit();
		}
	}
	
	@Override
	protected void onProgressUpdate(UploaderProgress... files) {
		super.onProgressUpdate(files);

		if ((mStatusUpdatePreference.length() > 0) && (context != null)) {
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
			SharedPreferences.Editor editor = settings.edit();
			for (int i = 0; i < files.length; i++) {
				editor.putString(mStatusUpdatePreference, files[i].toString());
			}
			editor.commit();
		}
	}
	
	@Override
	protected void onPostExecute(File[] result) {
		super.onPostExecute(result);
	}
}
