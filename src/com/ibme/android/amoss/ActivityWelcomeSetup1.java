// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.android.amoss;

import com.ibme.android.oxlith.R;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityWelcomeSetup1 extends PreferenceActivity {

	private static final String TAG = "AMoSSWelcomeSetup1";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Preference pref;
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences_welcome_setup_1);

		pref = (Preference)findPreference("buttonCheck");
		pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference arg0) {
				new ClassEvents(TAG, "INFO", "Checking server connection");
				new TaskCheckerIBME(getApplicationContext()).execute();
			    return true;
			}
		});
		
		final float scale = getResources().getDisplayMetrics().density;
		
        ListView v = getListView();
        
        TextView tv = new TextView(this);
        tv.setText("First we need to set up your OxLith study ID and password\n\nPlease enter your details and press Next to continue");
        tv.setTextAppearance(this, android.R.style.TextAppearance_Medium);
        tv.setPadding((int)(10 * scale), (int)(30 * scale), (int)(10 * scale), 0);
        tv.setGravity(Gravity.CENTER);
        v.addHeaderView(tv);
        
        Button b = new Button(this);
        b.setText("Next");
        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
        		Intent welcomeActivity = new Intent(getBaseContext(), ActivityWelcomeSetup2.class);
        		startActivity(welcomeActivity);
        		finish();
            }
        });
        v.addFooterView(b);
	}
	
	@Override
	public void onBackPressed() {
		Intent welcomeActivity = new Intent(getBaseContext(), ActivityWelcome.class);
		startActivity(welcomeActivity);
		finish();
	}
}
