// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

import com.ibme.android.oxlith.R;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;

public class ActivitySettings extends PreferenceActivityWithMenu implements OnSharedPreferenceChangeListener {

	//private static final String TAG = "AMoSSSettings";

	public ActivitySettings() {
		super(R.id.menu_settings);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Preference pref;
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);

		pref = findPreference("listMobileUsage");
		pref.setSummary(((ListPreference)pref).getEntry());
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if ((prefs.getBoolean("checkboxHighIntensity", false)) &&
				(prefs.getBoolean("checkboxHighIntensityMoodZoom", false))) {
			String strDisabledHighIntensity = getResources().getString(R.string.pref_mood_zoom_summary_disabled_high_intensity);
			pref = findPreference("checkboxMoodZoom");
			pref.setEnabled(false);
			pref.setSummary(pref.getSummary() + "\n" + strDisabledHighIntensity);
			pref = findPreference("scheduleMoodZoom");
			pref.setEnabled(false);
			pref.setSummary(pref.getSummary() + "\n" + strDisabledHighIntensity);
		} else {
			UpdateMoodZoomSchedule(prefs.getBoolean("checkboxMoodZoom", true));
		}
		
		// Custom configurations for OxLith
		pref = findPreference("checkboxMoodZoom");
		pref.setEnabled(false);
		
		pref = findPreference("listNotificationSound");
		pref.setEnabled(false);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		Preference pref = findPreference(key);
		if (key.equals("listMobileUsage")) {
			pref.setSummary(((ListPreference)pref).getEntry());
		} else if (key.equals("checkboxMoodZoom")) {
			boolean bValue = sharedPreferences.getBoolean("checkboxMoodZoom", true);
			UpdateMoodZoomSchedule(bValue);
		}
	}
	
	private void UpdateMoodZoomSchedule(boolean bMoodZoomEnabled) {
		Preference pref = findPreference("scheduleMoodZoom");
		pref.setEnabled(bMoodZoomEnabled);
		
		String strSummary = getResources().getString(R.string.pref_mood_zoom_schedule_summary);
		String strDisabled = getResources().getString(R.string.pref_mood_zoom_summary_disabled);
		
		if (bMoodZoomEnabled) {
			pref.setSummary(strSummary);
		} else {
			pref.setSummary(strSummary + "\n" + strDisabled);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}
}
